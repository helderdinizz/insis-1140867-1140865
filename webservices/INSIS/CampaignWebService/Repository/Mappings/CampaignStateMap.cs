﻿using System.Diagnostics.CodeAnalysis;

using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

using Isep.Insis.Models.State;

namespace Isep.Insis.Repository.Mappings
{
    public sealed class CampaignStateMap : ClassMap<CampaignState>
    {
        [SuppressMessage("CampaignStateMap", "CS0618")]
        public CampaignStateMap()
        {
            DefaultAccess.Property();
            Id(s => s.Id).GeneratedBy.Assigned();

            DiscriminateSubClassesOnColumn("State")
                .SubClass<CreatedCampaignState>(nameof(CreatedCampaignState), x => { })
                .SubClass<ApprovedCampaignState>(nameof(ApprovedCampaignState), x => { })
                .SubClass<ContentsDefinedCampaignState>(nameof(ContentsDefinedCampaignState), x => { })
                .SubClass<TargetsParameterizedCampaignState>(nameof(TargetsParameterizedCampaignState), x => { })
                .SubClass<ReadyForApprovalCampaignState>(nameof(ReadyForApprovalCampaignState), x => { })
                .SubClass<SentCampaignState>(nameof(SentCampaignState), x => { })
                .SubClass<NeedsContentsRedefinitionCampaignState>(nameof(NeedsContentsRedefinitionCampaignState), x => { })
                .SubClass<NeedsTargetReParameterizationAndContentRedefinitionCampaignState>(nameof(NeedsTargetReParameterizationAndContentRedefinitionCampaignState), x => { })
                .SubClass<NeedsTargetsReParameterizationCampaignState>(nameof(NeedsTargetsReParameterizationCampaignState), x => { })
                .SubClass<ScheduledCampaignState>(nameof(ScheduledCampaignState), x => { });
        }
    }
}