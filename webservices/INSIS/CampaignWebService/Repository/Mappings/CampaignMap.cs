﻿using FluentNHibernate.Mapping;

using Isep.Insis.Models;
using Isep.Insis.Models.State;

namespace Isep.Insis.Repository.Mappings
{
    public sealed class CampaignMap : ClassMap<Models.Campaign.Campaign>
    {
        public CampaignMap()
        {
            Id(campaign => campaign.Id).GeneratedBy.Guid();

            Map(campaign => campaign.Description);
            Map(campaign => campaign.Objective);
            Map(campaign => campaign.SendAtUtc);

            Component(
                campaign => campaign.Contents,
                catalog =>
                {
                    catalog.HasMany(content => content.Catalog)
                    .Cascade.All()
                    .Table("CampaignContentDefinitions")
                    .AsBag()
                    .Component(content =>
                        {
                            content.Map(x => x.Channel);
                            content.Map(x => x.Content);
                            content.Map(x => x.Selected);
                        })
                    .Not.LazyLoad();
                    catalog.Not.LazyLoad();
                });

            Component(campaign => campaign.TargetClientParameters, parameters =>
            {
                parameters.HasMany(parameter => parameter.Countries)
       .Not.LazyLoad()
       .Cascade.All()
       .Table("CampaignCountry")
       .Element("Country", element => element.Type<GenericEnumMapper<Country>>());

                parameters.Component(p => p.AgeRange, ageRange =>
                {
                    ageRange.Map(ar => ar.MinValue, "MinAge");
                    ageRange.Map(ar => ar.MaxValue, "MaxAge");
                    ageRange.Map(ar => ar.Inclusive, "InclusiveAgeRange");
                });

                parameters.Not.LazyLoad();
            });

            References(campaign => campaign.State)
                .Class<CampaignState>()
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .Not.LazyLoad()
                .Fetch.Join();
        }
    }
}