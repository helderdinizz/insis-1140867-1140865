﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Isep.Insis.Models;

using NHibernate;

namespace Isep.Insis.Repository
{
    public abstract class GenericRepository<TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : class, IEntity<TId>
    {
        protected readonly ISessionFactory _sessionFactory;

        public GenericRepository(ISessionFactory sessionFactory)
        {
            this._sessionFactory = sessionFactory;
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                await session.SaveAsync(entity);
                await session.FlushAsync();
            }
            return entity;
        }

        public virtual async Task DeleteAsync(TId id)
        {
            var entity = await GetAsync(id);
            if (entity == null)
            {
                throw new InvalidOperationException($"Entity with id {id} not found.");
            }
            using (var session = _sessionFactory.OpenSession())
            {
                await session.DeleteAsync(entity);
                await session.FlushAsync();
            }
        }

        public virtual Task<IEnumerable<TEntity>> GetAll()
        {
            using (var session = _sessionFactory.OpenSession())
            {
                return Task.FromResult(session.Query<TEntity>().ToList().AsEnumerable());
            }
        }

        public virtual async Task<TEntity> GetAsync(TId id)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                return await session.GetAsync<TEntity>(id);
            }
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                await session.UpdateAsync(entity);
                await session.FlushAsync();
            }

            return entity;
        }

        public virtual void Dispose() => GC.SuppressFinalize(this);
    }
}