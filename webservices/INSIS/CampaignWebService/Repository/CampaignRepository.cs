﻿using System;

using NHibernate;

namespace Isep.Insis.Repository
{
    public class CampaignRepository : GenericRepository<Models.Campaign.Campaign, Guid>, ICampaignRepository
    {
        public CampaignRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }
    }
}