﻿using System.Threading.Tasks;

using Isep.Insis.Models;

namespace Isep.Insis.Repository
{
    internal interface IStatefulEntityRepository<TState, TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : class, IStatefulEntity<TState, TId>
    {
        Task<bool> SwitchStateAsync(TEntity entity, TState state);
    }
}