﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Isep.Insis.Helpers;
using Isep.Insis.Models;

namespace Isep.Insis.Repository
{
    public class InMemoryUserRepository : IUserRepository
    {
        private readonly ICampaignRepository _campaignRepository;

        private static readonly List<CampaignTarget> Users = new List<CampaignTarget>()
        {
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07df4"),"Portugal", 23, "silvagomesandre94@gmail.com", "andresilva", "+351912031219", (DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("6dfe3dba-2375-4ca8-a63d-15c3bc0c8946"),"Portugal", 26, "silvagomesandre94@gmail.com", "andresilva6", "+351912031219", (DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07df5"),"UnitedKingdom", 22, "silvagomesandre94@gmail.com", "andresilva2", "+351912031219", (DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07df6"),"Portugal", 12, "silvagomesandre94@gmail.com", "andresilva3", "+351912031219", (DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07df7"),"Spain", 67, "silvagomesandre94@gmail.com", "andresilva4", "+351912031219",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07df8"),"Spain", 42, "silvagomesandre94@gmail.com", "andresilva5", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07df9"),"Portugal", 50, "silvagomesandre94@gmail.com", "andresilva6", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da1"),"UnitedKingdom", 49, "silvagomesandre94@gmail.com", "andresilva7", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da2"),"Russia", 46, "silvagomesandre94@gmail.com", "andresilva8", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da3"),"UnitedKingdom", 24, "silvagomesandre94@gmail.com", "andresilva9", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da4"),"UnitedStates", 80, "silvagomesandre94@gmail.com", "andresilva10", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da5"),"Russia", 56, "silvagomesandre94@gmail.com", "andresilva11", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da6"),"France", 33, "silvagomesandre94@gmail.com", "andresilva12", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da7"),"France", 32, "silvagomesandre94@gmail.com", "andresilva13", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
            new CampaignTarget(Guid.Parse("3968f9c6-3903-455a-8cc2-6e3b25a07da8"),"China", 18, "silvagomesandre94@gmail.com", "andresilva14", "+351915138899",(DispatchChannel[])Enum.GetValues(typeof(DispatchChannel))),
        };

        public InMemoryUserRepository(ICampaignRepository campaignRepository)
        {
            this._campaignRepository = campaignRepository;
        }

        public Task<CampaignTarget> CreateAsync(CampaignTarget entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }

        public Task<IEnumerable<CampaignTarget>> GetAll()
        {
            return Task.FromResult(Users.AsEnumerable());
        }

        public Task<IEnumerable<CampaignTarget>> GetAllForCampaign(Models.Campaign.Campaign campaign)
        {
            if (campaign?.State.IsInApproved() == true || campaign?.State.IsInReadyForApproval() == true)
            {
                return Task.FromResult(Users.Where(u => campaign.TargetClientParameters.Countries.Contains(u.Country)
                    && campaign.TargetClientParameters.AgeRange.Matches(u.Age)));
            }
            throw new InvalidCampaignOperationException("Campaign does not exist or must be either approved or ready for approval in order to gather the target clients.");
        }

        public Task<IEnumerable<CampaignTarget>> GetAllForCampaignAndChannel(Models.Campaign.Campaign campaign, DispatchChannel channel)
        {
            if (campaign?.State.IsInApproved() == true || campaign?.State.IsInReadyForApproval() == true)
            {
                return Task.FromResult(Users.Where(u => campaign.TargetClientParameters.Countries.Contains(u.Country)
                    && campaign.TargetClientParameters.AgeRange.Matches(u.Age)
                    && u.SubscribedChannels.Contains(channel)));
            }
            throw new InvalidCampaignOperationException("Campaign does not exist or must be either approved or ready for approval in order to gather the target clients.");
        }

        public Task<CampaignTarget> GetAsync(Guid id)
        {
            return Task.FromResult(Users.Find(c => c.Id.Equals(id)));
        }

        public Task<CampaignTarget> UpdateAsync(CampaignTarget entity)
        {
            Users.RemoveAt(Users.IndexOf(Users.FirstOrDefault(u => u.Id == entity.Id)));
            Users.Add(entity);
            return Task.FromResult(entity);
        }
    }
}