﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Isep.Insis.Repository
{
    public interface IRepository<TEntity, TId> : IDisposable where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAll();

        Task<TEntity> GetAsync(TId id);

        Task<TEntity> CreateAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);

        Task DeleteAsync(TId id);
    }
}