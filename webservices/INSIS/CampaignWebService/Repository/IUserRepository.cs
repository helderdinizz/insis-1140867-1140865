﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Isep.Insis.Models;

namespace Isep.Insis.Repository
{
    public interface IUserRepository : IRepository<CampaignTarget, Guid>
    {
        Task<IEnumerable<CampaignTarget>> GetAllForCampaign(Models.Campaign.Campaign id);
        Task<IEnumerable<CampaignTarget>> GetAllForCampaignAndChannel(Models.Campaign.Campaign id, DispatchChannel channel);
    }
}