﻿using System;

namespace Isep.Insis.Repository
{
    public interface ICampaignRepository : IRepository<Models.Campaign.Campaign, Guid>
    {
    }
}