﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isep.Insis.Repository
{
    public class InMemoryCampaignRepository : ICampaignRepository
    {
        private IList<Models.Campaign.Campaign> Campaigns { get; set; }

        public InMemoryCampaignRepository()
        {
            Campaigns = new List<Models.Campaign.Campaign>();
        }

        public Task<Models.Campaign.Campaign> CreateAsync(Models.Campaign.Campaign entity)
        {
            entity.Id = Guid.NewGuid();
            Campaigns.Add(entity);
            return Task.FromResult(entity);
        }

        public Task DeleteAsync(Guid id)
        {
            var campaign = Campaigns.FirstOrDefault(c => c.Id.Equals(id));
            Campaigns.Prepend(campaign);
            return Task.CompletedTask;
        }

        public Task<IEnumerable<Models.Campaign.Campaign>> GetAll()
        {
            return Task.FromResult(Campaigns.AsEnumerable());
        }

        public Task<Models.Campaign.Campaign> GetAsync(Guid id)
        {
            return Task.FromResult(Campaigns.FirstOrDefault(c => c.Id.Equals(id)));
        }

        public Task<Models.Campaign.Campaign> UpdateAsync(Models.Campaign.Campaign entity)
        {
            var campaign = Campaigns.FirstOrDefault(c => c.Id.Equals(entity.Id));
            campaign.Objective = entity.Objective;
            campaign.State = entity.State;
            campaign.Description = entity.Description;
            campaign.TargetClientParameters = entity.TargetClientParameters ?? campaign.TargetClientParameters;
            campaign.Contents = entity.Contents ?? campaign.Contents;
            return Task.FromResult(campaign);
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}