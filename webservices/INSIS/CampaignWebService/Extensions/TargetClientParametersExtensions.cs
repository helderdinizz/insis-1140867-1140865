﻿using System;
using System.Linq;

using Isep.Insis.Models;

namespace Isep.Insis.Extensions
{
    public static class TargetClientParametersExtensions
    {
        public static TargetClientParameters ToModel(this DTO.TargetClientParameters parameters)
        {
            if (parameters == null) throw new ArgumentException("Campaign Parameters cannot be null.", "parameters");
            return new TargetClientParameters(
                parameters.Countries?.Select(country =>
                {
                    if (!Enum.TryParse(country, out Country countryEnum))
                    {
                        throw new InvalidOperationException($"The country {country} is not recognized by this application.");
                    }
                    return countryEnum;
                }) ?? Enumerable.Empty<Country>(),
                new Range<int>(parameters.MinAge, parameters.MaxAge, true));
        }

        public static DTO.TargetClientParameters ToDTO(this TargetClientParameters parameters)
        {
            if (parameters == null) throw new ArgumentException("Campaign Parameters cannot be null.", "parameters");
            return new DTO.TargetClientParameters()
            {
                MinAge = parameters.AgeRange.MinValue,
                MaxAge = parameters.AgeRange.MaxValue,
                Countries = parameters.Countries?.Select(countryEnum => countryEnum.ToString()) ?? Enumerable.Empty<string>()
            };
        }
    }
}