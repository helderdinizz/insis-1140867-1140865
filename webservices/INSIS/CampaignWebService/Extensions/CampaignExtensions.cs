﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Isep.Insis.Extensions
{
    public static class CampaignExtensions
    {
        public static Models.Campaign.Campaign ToModel(this DTO.Campaign campaign)
        {
            if (campaign == null) throw new ArgumentException("Campaign Model cannot be null.", "campaign");
            return new Models.Campaign.Campaign()
            {
                Id = campaign.Id,
                TargetClientParameters = campaign.TargetClientParameters?.ToModel() ?? new Models.TargetClientParameters(),
                Description = campaign.Description,
                Objective = campaign.Objective,
                Contents = campaign.Contents?.ToModel() ?? new Models.Campaign.CampaignContentCatalog(),
                SendAtUtc = campaign.ParsedSendAtUtc
            };
        }

        public static DTO.Campaign ToDTO(this Models.Campaign.Campaign campaign)
        {
            if (campaign == null) throw new ArgumentException("Campaign DTO cannot be null.", "campaign");
            return new DTO.Campaign()
            {
                Id = campaign.Id,
                Contents = campaign.Contents?.ToDTO(),
                Description = campaign.Description,
                TargetClientParameters = campaign.TargetClientParameters?.ToDTO(),
                Objective = campaign.Objective,
                State = campaign.State?.ToString(),
                SendAtUtc = campaign.SendAtUtc == default(DateTime) ? null : campaign.SendAtUtc.ToString("yyyy-MM-dd HH:mm")
            };
        }

        public static IEnumerable<Models.Campaign.Campaign> ToModel(this IEnumerable<DTO.Campaign> campaigns)
        {
            if (campaigns == null) throw new ArgumentException("Campaigns cannot be null.", "campaigns");
            return campaigns.Select(c => c.ToModel());
        }

        public static IEnumerable<DTO.Campaign> ToDTO(this IEnumerable<Models.Campaign.Campaign> campaigns)
        {
            if (campaigns == null) throw new ArgumentException("Campaigns cannot be null.", "campaigns");
            return campaigns.Select(c => c.ToDTO());
        }
    }
}