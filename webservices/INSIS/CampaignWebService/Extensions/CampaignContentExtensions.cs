﻿using System.Collections.Generic;
using System.Linq;

namespace Isep.Insis.Extensions
{
    public static class CampaignContentExtensions
    {
        public static Models.Campaign.CampaignContent ToModel(this DTO.CampaignContent content)
        {
            return new Models.Campaign.CampaignContent(content.Channel, content.Content, content.Selected);
        }

        public static DTO.CampaignContent ToDTO(this Models.Campaign.CampaignContent content)
        {
            return new DTO.CampaignContent()
            {
                Channel = content.Channel,
                Content = content.Content,
                Selected = content.Selected,
            };
        }

        public static Models.Campaign.CampaignContentCatalog ToModel(this IEnumerable<DTO.CampaignContent> content)
        {
            return new Models.Campaign.CampaignContentCatalog(content.Select(c => c.ToModel()));
        }

        public static IEnumerable<DTO.CampaignContent> ToDTO(this Models.Campaign.CampaignContentCatalog content)
        {
            return content.Select(c => c.ToDTO());
        }
    }
}