﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using Isep.Insis.Models;

using NodaTime;

namespace Isep.Insis.Extensions
{
    public static class CampaignTargetExtensions
    {
        public static CampaignTarget ToModel(this DTO.CampaignTarget target)
        {
            if (target == null) throw new ArgumentException("Campaign Target cannot be null.", nameof(target));
            return new CampaignTarget(target.Id, target.Country, target.Age, target.Email, target.Username, target.PhoneNumber, target.SubscribedChannels);
        }

        public static DTO.CampaignTarget ToDTO(this CampaignTarget target)
        {
            if (target == null) throw new ArgumentException("Campaign Target cannot be null.", nameof(target));
            return new DTO.CampaignTarget()
            {
                Id = target.Id,
                Username = target.Username,
                Email = target.Email,
                Age = target.Age,
                Country = target.Country.ToString(),
                LocalTime = target.Country.GetLocalTime().ToString("yyyy-MM-ddTHH:mm"),
                TimeZone = target.Country.GetTimeZone().Id,
                PhoneNumber = target.PhoneNumber,
                SubscribedChannels = target.SubscribedChannels
            };
        }

        public static DTO.CampaignTarget ToDTO(this CampaignTarget target, DateTime campaignSendTime)
        {
            if (target == null) throw new ArgumentException("Campaign Target cannot be null.", nameof(target));
            if (campaignSendTime == default(DateTime)) throw new ArgumentException("Campaign send time cannot be empty.", nameof(campaignSendTime));

            var specificUserTimeZoneSendTime = LocalDateTime.FromDateTime(campaignSendTime)
                .InZoneStrictly(target.Country.GetTimeZone())
                .ToInstant()
                .InZone(Country.Portugal.GetTimeZone());

            return new DTO.CampaignTarget()
            {
                Id = target.Id,
                Username = target.Username,
                Email = target.Email,
                Age = target.Age,
                Country = target.Country.ToString(),
                LocalTime = target.Country.GetLocalTime().ToString("yyyy-MM-ddTHH:mm"),
                TimeZone = target.Country.GetTimeZone().Id,
                SendAt = specificUserTimeZoneSendTime.ToString("yyyy-MM-ddTHH:mm", CultureInfo.InvariantCulture),
                PhoneNumber = target.PhoneNumber
            };
        }

        public static IEnumerable<CampaignTarget> ToModel(this IEnumerable<DTO.CampaignTarget> targets)
        {
            if (targets == null) throw new ArgumentException("Campaign Target cannot be null.", "target");
            return targets.Select(c => c.ToModel());
        }

        public static IEnumerable<DTO.CampaignTarget> ToDTO(this IEnumerable<CampaignTarget> targets)
        {
            if (targets == null) throw new ArgumentException("Campaign Target cannot be null.", "target");
            return targets.Select(c => c.ToDTO());
        }

        public static IEnumerable<DTO.CampaignTarget> ToDTO(this IEnumerable<CampaignTarget> targets, DateTime campaignSendTime)
        {
            if (targets == null) throw new ArgumentException("Campaign Target cannot be null.", "target");
            return targets.Select(c => c.ToDTO(campaignSendTime)).OrderBy(c => c.SendAt);
        }
    }
}