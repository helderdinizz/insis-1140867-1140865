﻿using System;
using System.ComponentModel;
using System.Linq;

using Isep.Insis.Helpers;

using NodaTime;

namespace Isep.Insis.Models
{
    public enum Country
    {
        None,

        [CountryInfo("Europe/Lisbon")]
        Portugal,

        [CountryInfo("Europe/Madrid")]
        Spain,

        [CountryInfo("Europe/Paris")]
        France,

        [CountryInfo("Europe/London")]
        [Description("United Kingdom")]
        UnitedKingdom,

        [CountryInfo("Europe/Berlin")]
        Germany,

        [CountryInfo("America/New_York")]
        [Description("United States")]
        UnitedStates,

        [CountryInfo("America/Sao_Paulo")]
        Brazil,

        [CountryInfo("America/Argentina")]
        Argentina,

        [CountryInfo("Asia/Shanghai")]
        China,

        [CountryInfo("Asia/Japan")]
        Japan,

        [CountryInfo("Europe/Moscow")]
        Russia
    }

    public static class CountryExtensions
    {
        public static string GetName(this Country country)
        {
            var description = country.GetType().GetCustomAttributes(typeof(DescriptionAttribute), false).SingleOrDefault() as DescriptionAttribute;
            if (description != null && !string.IsNullOrEmpty(description.Description))
            {
                return description.Description;
            }
            return country.ToString();
        }

        public static DateTime GetLocalTime(this Country country)
        {
            var timeZone = country.GetType().GetMember(country.ToString()).Single().GetCustomAttributes(typeof(CountryInfoAttribute), false).SingleOrDefault() as CountryInfoAttribute;
            if (timeZone != null)
            {
                return timeZone.CurrentTime;
            }
            return DateTime.UtcNow;
        }

        public static DateTimeZone GetTimeZone(this Country country)
        {
            var timeZone = country.GetType().GetMember(country.ToString()).Single().GetCustomAttributes(typeof(CountryInfoAttribute), false).SingleOrDefault() as CountryInfoAttribute;
            if (timeZone != null)
            {
                return timeZone.TimeZoneInfo;
            }
            return DateTimeZone.Utc;
        }
    }
}