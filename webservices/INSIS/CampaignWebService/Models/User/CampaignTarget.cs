﻿using System;
using System.Collections.Generic;

namespace Isep.Insis.Models
{
    public class CampaignTarget : IUser
    {
        public Guid Id { get; set; }

        public Country Country { get; set; }

        public int Age { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<DispatchChannel> SubscribedChannels { get; set; }

        public CampaignTarget(Guid id, string country, int age, string email, string username, string phoneNumber, IEnumerable<DispatchChannel> subscribedChannels)
        {
            if (!Enum.TryParse(country, out Country countryEnum))
            {
                throw new ArgumentException($"Country {country} is not valid.", nameof(country));
            }
            this.Id = id;
            this.Country = countryEnum;
            this.Age = age;
            this.Email = email;
            this.Username = username;
            this.PhoneNumber = phoneNumber;
            this.SubscribedChannels = subscribedChannels;

        }
    }
}