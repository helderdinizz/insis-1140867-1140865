﻿using Isep.Insis.Models.State;

namespace Isep.Insis.Models
{
    public interface ICampaignOperation
    {
        ICampaignState ResultingState();

        bool IsCreateCampaign();

        bool IsDefineContents();

        bool IsApproveCampaign();

        bool IsTargetParameterization();

        bool IsCampaignScheduled();

        bool IsSendCampaign();
    }

    public abstract class CampaignOperation : ICampaignOperation
    {
        public virtual bool IsApproveCampaign() => false;

        public virtual bool IsCampaignScheduled() => false;

        public virtual bool IsCreateCampaign() => false;

        public virtual bool IsDefineContents() => false;

        public virtual bool IsSendCampaign() => false;

        public virtual bool IsTargetParameterization() => false;

        public abstract ICampaignState ResultingState();

        public override string ToString()
        {
            return GetType().Name;
        }
    }

    public class CreateCampaignOperation : CampaignOperation
    {
        public override bool IsCreateCampaign() => true;

        public override ICampaignState ResultingState()
        {
            return new CreatedCampaignState();
        }
    }

    public class DefinitionOfContentsCampaignOperation : CampaignOperation
    {
        public override bool IsDefineContents() => true;

        public override ICampaignState ResultingState()
        {
            return new ContentsDefinedCampaignState();
        }
    }

    public class ApproveCampaignOperation : CampaignOperation
    {
        public override bool IsApproveCampaign() => true;

        public override ICampaignState ResultingState()
        {
            return new ApprovedCampaignState();
        }
    }

    public class TargetParameterizationCampaignOperation : CampaignOperation
    {
        public override bool IsTargetParameterization() => true;

        public override ICampaignState ResultingState()
        {
            return new TargetsParameterizedCampaignState();
        }
    }

    public class SendCampaignOperation : CampaignOperation
    {
        public override bool IsSendCampaign() => true;

        public override ICampaignState ResultingState()
        {
            return new SentCampaignState();
        }
    }

    public class ScheduledCampaignOperation : CampaignOperation
    {
        public override bool IsCampaignScheduled() => true;

        public override ICampaignState ResultingState()
        {
            return new ScheduledCampaignState();
        }
    }
}