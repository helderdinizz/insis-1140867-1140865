﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public abstract class StatefulEntity<TState, TId> : IStatefulEntity<TState, TId>
        where TState : IState
    {
        private TState _state;

        public StatefulEntity(TState state)
        {
            this._state = state;
        }

        public virtual TState State
        {
            get { return _state; }
            set
            {
                if (_state == null)
                {
                    _state = value;
                }
                else if (_state.CanSwitchTo(value.GetType()))
                {
                    _state = value;
                }
                else throw new InvalidCampaignOperationException($"Cannot switch from state {State.Name} to {value.Name}");
            }
        }

        public virtual TId Id { get; set; }
    }
}