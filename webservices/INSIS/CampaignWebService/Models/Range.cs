﻿using System;
using System.Collections.Generic;

namespace Isep.Insis.Models
{
    public struct Range<TBaseType> : IEquatable<Range<TBaseType>>, IComparable<Range<TBaseType>>
        where TBaseType : IEquatable<TBaseType>, IComparable<TBaseType>
    {
        public static bool operator ==(Range<TBaseType> x, Range<TBaseType> y)
        {
            return x.Equals(y);
        }

        public static bool operator !=(Range<TBaseType> x, Range<TBaseType> y)
        {
            return !(x == y);
        }

        public static bool operator >(Range<TBaseType> x, Range<TBaseType> y)
        {
            return x.CompareTo(y) > 0;
        }

        public static bool operator <(Range<TBaseType> x, Range<TBaseType> y)
        {
            return x.CompareTo(y) < 0;
        }

        public TBaseType MinValue { get; }
        public TBaseType MaxValue { get; }
        public bool Inclusive { get; }

        public Range(TBaseType minValue, TBaseType maxValue, bool inclusive) : this()
        {
            if (inclusive)
            {
                if (minValue.CompareTo(maxValue) > 0)
                {
                    throw new ArgumentOutOfRangeException("minValue", "Min value cannot be higher than Max value.");
                }
            }
            else
            {
                if (minValue.CompareTo(maxValue) >= 0)
                {
                    throw new ArgumentOutOfRangeException("minValue", "Min value cannot be higher or equal than Max value.");
                }
            }

            MinValue = minValue;
            MaxValue = maxValue;
            Inclusive = inclusive;
        }

        public bool Equals(Range<TBaseType> other)
        {
            return MinValue.Equals(other.MinValue) && MaxValue.Equals(other.MaxValue) && Inclusive.Equals(other.Inclusive);
        }

        public bool Matches(TBaseType source)
        {
            return Inclusive ?
                MinValue.CompareTo(source) <= 0 && MaxValue.CompareTo(source) >= 0 :
                MinValue.CompareTo(source) < 0 && MaxValue.CompareTo(source) > 0;
        }

        public int CompareTo(Range<TBaseType> other)
        {
            return (MaxValue.GetHashCode() - MaxValue.GetHashCode()) - (other.MaxValue.GetHashCode() - other.MinValue.GetHashCode());
        }

        public override int GetHashCode()
        {
            var hashCode = 745661832;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<TBaseType>.Default.GetHashCode(MinValue);
            hashCode = hashCode * -1521134295 + EqualityComparer<TBaseType>.Default.GetHashCode(MaxValue);
            hashCode = hashCode * -1521134295 + Inclusive.GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            return obj is Range<TBaseType> objRange && Equals(objRange);
        }

        public override string ToString()
        {
            return $"{MinValue} : {MaxValue} : {(Inclusive ? "Inclusive" : "Exclusive")}";
        }
    }
}