﻿namespace Isep.Insis.Models
{
    public enum DispatchChannel
    {
        Email, SMS, SocialNetwork
    }
}