﻿using System;

using Isep.Insis.Models.State;

namespace Isep.Insis.Models.Campaign
{
    public class Campaign : StatefulEntity<ICampaignState, Guid>
    {
        public Campaign() : this(null)
        {
        }

        public Campaign(ICampaignState state) : base(state)
        {
        }

        public virtual string Description { get; set; }

        public virtual string Objective { get; set; }

        public virtual DateTime SendAtUtc { get; set; }

        public virtual CampaignContentCatalog Contents { get; set; }

        public virtual TargetClientParameters TargetClientParameters { get; set; }
    }
}