﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Isep.Insis.Models.Campaign
{
    public class CampaignContentCatalog : IValueObject<CampaignContentCatalog>, IEnumerable<CampaignContent>
    {
        public virtual IList<CampaignContent> Catalog { get; }

        public CampaignContentCatalog()
        {
            Catalog = new List<CampaignContent>();
        }

        public CampaignContentCatalog(IEnumerable<CampaignContent> campaignContentList)
        {
            Catalog = campaignContentList.ToList();
        }

        public IEnumerator<CampaignContent> GetEnumerator()
        {
            if (Catalog == null)
            {
                return Enumerable.Empty<CampaignContent>().GetEnumerator();
            }
            return Catalog.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (var content in Catalog)
            {
                content.GetObjectData(info, context);
            }
        }

        public bool Equals(CampaignContentCatalog other)
        {
            return Catalog
            .OrderBy(x => x.Channel)
            .SequenceEqual(other.Catalog.OrderBy(x => x.Channel));
        }

        public CampaignContentCatalog SetSelectedContent(DispatchChannel channel, bool selected)
        {
            var existingCampaignContent = Catalog.SingleOrDefault(cc => cc.Channel == channel);

            if (existingCampaignContent != null)
            {
                Catalog.Remove(existingCampaignContent);
                Catalog.Add(new CampaignContent(existingCampaignContent.Channel, existingCampaignContent.Content, selected));
            }
            else
            {
                Catalog.Add(new CampaignContent(channel, null, selected));
            }
            return this;
        }

        public CampaignContentCatalog AddCampaignContent(DispatchChannel channel, string content)
        {
            var existingCampaignContent = Catalog.SingleOrDefault(cc => cc.Channel == channel);

            if (existingCampaignContent != null)
            {
                Catalog.Remove(existingCampaignContent);
                Catalog.Add(new CampaignContent(channel, content, existingCampaignContent.Selected));
            }
            else
            {
                Catalog.Add(new CampaignContent(channel, content, default(bool)));
            }
            return this;
        }

        public static explicit operator CampaignContentCatalog(string catalog)
        {
            if (!catalog.Trim().Any()) return new CampaignContentCatalog(Enumerable.Empty<CampaignContent>());
            List<CampaignContent> catalogs = catalog.Split('|')
                .Select(x => (CampaignContent)x)
                .ToList();
            return new CampaignContentCatalog(catalogs);
        }

        public static implicit operator string(CampaignContentCatalog catalog)
        {
            if (catalog == null) return "";
            return string.Join('|', catalog.Select(x => (string)x));
        }
    }
}