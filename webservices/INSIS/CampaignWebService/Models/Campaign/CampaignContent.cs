﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Isep.Insis.Models.Campaign
{
    public class CampaignContent : IValueObject<CampaignContent>
    {
        public CampaignContent()
        {
        }

        public virtual DispatchChannel Channel { get; }

        public virtual string Content { get; }

        public virtual bool Selected { get; }

        public CampaignContent(DispatchChannel channel, string content, bool selected)
        {
            this.Channel = channel;
            this.Content = content;
            this.Selected = selected;
        }

        public CampaignContent(string channel, string content, bool selected) : this((DispatchChannel)Enum.Parse(typeof(DispatchChannel), channel), content, selected)
        {
        }

        public bool Equals(CampaignContent other)
        {
            if (other == null) return false;
            if (!other.Channel.Equals(this.Channel)) return false;
            return other.Content?.Equals(this.Content) ?? this.Content == null ? true : false;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Content", Content, typeof(string));
            info.AddValue("Channel", Channel.ToString(), typeof(string));
        }

        public override int GetHashCode()
        {
            var hashCode = -924886970;
            hashCode = hashCode * -1521134295 + Channel.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Content);
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CampaignContent);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public static bool operator ==(CampaignContent a, CampaignContent b)
        {
            if (a is null && b is null)
                return true;
            if (a is null || b is null)
                return false;

            return a.Equals(b);
        }

        public static explicit operator string(CampaignContent v)
        {
            return v.Content + "|" + v.Channel.ToString();
        }

        public static explicit operator CampaignContent(string v)
        {
            var str = v.Split("|");
            return new CampaignContent(str[2], str[1], bool.Parse(str[0]));
        }

        public static bool operator !=(CampaignContent a, CampaignContent b)
        {
            return !(a == b);
        }
    }
}