﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Isep.Insis.Models
{
    public class TargetClientParameters : IValueObject<TargetClientParameters>
    {
        public TargetClientParameters()
        {
        }

        public TargetClientParameters(IEnumerable<Country> countries, Range<int> ageRange)
        {
            this.Countries = countries;
            this.AgeRange = ageRange;
        }

        public virtual IEnumerable<Country> Countries { get; }

        public virtual Range<int> AgeRange { get; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Country", Countries, typeof(IEnumerable<Country>));
            info.AddValue("MinAge", AgeRange.MinValue, typeof(int));
            info.AddValue("MaxAge", AgeRange.MaxValue, typeof(int));
        }

        public bool Equals(TargetClientParameters other)
        {
            if (other == null) return false;
            if (!other.Countries.OrderBy(c => c.ToString()).SequenceEqual(this.Countries.OrderBy(c => c.ToString()))) return false;
            if (!other.AgeRange.Equals(this.AgeRange)) return false;
            return true;
        }
    }
}