﻿namespace Isep.Insis.Models
{
    internal interface IStatefulEntity<TState, IId> : IEntity<IId>
    {
        TState State { get; set; }
    }
}