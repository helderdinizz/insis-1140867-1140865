﻿using System;

namespace Isep.Insis.Models.State
{
    public interface IState
    {
        string Name { get; }

        bool CanSwitchTo(Type state);

        IState NextStateAfter(Type justExecutedOperation);
    }
}