﻿using Isep.Insis.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isep.Insis.Models.State
{
    public class ScheduledCampaignState : CampaignState
    {
        public ScheduledCampaignState() : base(10)
        {

        }

        public override string Name => "Scheduled";

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInScheduled() || state.IsInSent();
        }

        public override bool IsInScheduled() => true;

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsCampaignScheduled())
            {
                return justExecutedOperation.ResultingState();
            }

            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}
