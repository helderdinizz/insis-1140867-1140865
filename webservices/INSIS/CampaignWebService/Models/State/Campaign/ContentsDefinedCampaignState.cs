﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class ContentsDefinedCampaignState : CampaignState
    {
        public override string Name => "ContentsDefined";

        public ContentsDefinedCampaignState() : base(3)
        {
        }

        public override bool IsInContentsDefined() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInContentsDefined() || state.IsInReadyForApproval();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsTargetParameterization())
            {
                return new ReadyForApprovalCampaignState();
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}