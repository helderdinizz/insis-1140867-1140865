﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class ReadyForApprovalCampaignState : CampaignState
    {
        public override string Name => "ReadyForApproval";

        public ReadyForApprovalCampaignState() : base(5)
        {
        }

        public override bool IsInReadyForApproval() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInReadyForApproval()
                || state.IsInApproved()
                || state.NeedsContentRedefinition()
                || state.NeedsContentRedefinitionAndTargetReParameterization()
                || state.NeedsTargetReParameterization();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsSendCampaign())
            {
                return justExecutedOperation.ResultingState();
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}