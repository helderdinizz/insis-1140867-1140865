﻿namespace Isep.Insis.Models.State
{
    public interface ICampaignState : IState, IEntity<int>
    {
        bool IsInCreated();

        bool IsInContentsDefined();

        bool IsInContentRedefinition();

        bool IsInTargetsParameterized();

        bool NeedsTargetReParameterization();

        bool NeedsContentRedefinition();

        bool NeedsContentRedefinitionAndTargetReParameterization();

        bool IsInReadyForApproval();

        bool IsInScheduled();

        bool IsInApproved();

        bool IsInSent();

        bool CanSwitchTo(ICampaignState state);

        ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation);
    }
}