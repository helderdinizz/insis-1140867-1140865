﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class CreatedCampaignState : CampaignState
    {
        public override string Name => "Created";

        public CreatedCampaignState() : base(2)
        {
        }

        public override bool IsInCreated() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInCreated()
                || state.IsInContentsDefined()
                || state.IsInTargetsParameterized();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsDefineContents()
                || justExecutedOperation.IsTargetParameterization())
            {
                return justExecutedOperation.ResultingState();
            }
            if (justExecutedOperation.IsCreateCampaign())
            {
                return this;
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}