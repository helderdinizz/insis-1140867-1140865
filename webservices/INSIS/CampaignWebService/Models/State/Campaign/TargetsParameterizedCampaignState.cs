﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class TargetsParameterizedCampaignState : CampaignState
    {
        public override string Name => "TargetsParameterized";

        public TargetsParameterizedCampaignState() : base(4)
        {
        }

        public override bool IsInTargetsParameterized() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInTargetsParameterized() || state.IsInReadyForApproval();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsDefineContents())
            {
                return new ReadyForApprovalCampaignState();
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}