﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class SentCampaignState : CampaignState
    {
        public override string Name => "Sent";

        public SentCampaignState() : base(6)
        {
        }

        public override bool IsInSent() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInSent();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}