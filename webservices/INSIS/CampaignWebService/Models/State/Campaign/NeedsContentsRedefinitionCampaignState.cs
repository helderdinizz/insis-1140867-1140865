﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class NeedsContentsRedefinitionCampaignState : CampaignState
    {
        public override string Name => "NeedsContentsRedefinition";

        public NeedsContentsRedefinitionCampaignState() : base(7)
        {
        }

        public override bool NeedsContentRedefinition() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.NeedsContentRedefinition() || state.IsInReadyForApproval();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsDefineContents())
            {
                return new ReadyForApprovalCampaignState();
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}