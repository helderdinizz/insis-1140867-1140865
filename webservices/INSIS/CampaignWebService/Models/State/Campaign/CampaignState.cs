﻿using System;

using Isep.Insis.Helpers;

using Newtonsoft.Json;

namespace Isep.Insis.Models.State
{
    public abstract class CampaignState : ICampaignState
    {
        [JsonIgnore]
        public virtual int Id { get; set; }

        public abstract string Name { get; }

        public CampaignState(int id)
        {
            this.Id = id;
        }

        public CampaignState()
        {
        }

        public virtual bool IsInApproved() => false;

        public virtual bool IsInCreated() => false;

        public virtual bool IsInContentsDefined() => false;

        public virtual bool IsInContentRedefinition() => false;

        public virtual bool IsInTargetsParameterized() => false;

        public virtual bool IsInTargetReparameterization() => false;

        public virtual bool IsInReadyForApproval() => false;

        public virtual bool IsInSent() => false;

        public virtual bool IsInScheduled() => false;

        public virtual bool NeedsTargetReParameterization() => false;

        public virtual bool NeedsContentRedefinition() => false;

        public virtual bool NeedsContentRedefinitionAndTargetReParameterization() => false;

        public virtual bool CanSwitchTo(Type state)
        {
            if (IsValidCampaignState(state))
            {
                return CanSwitchTo(Activator.CreateInstance(state) as ICampaignState);
            }
            return false;
        }

        public virtual IState NextStateAfter(Type justExecutedOperation)
        {
            if (IsValidCampaignOperation(justExecutedOperation))
            {
                return NextStateAfter(Activator.CreateInstance(justExecutedOperation) as ICampaignOperation);
            }
            throw new InvalidCampaignOperationException();
        }

        public abstract ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation);

        public virtual bool CanSwitchTo(IState state)
        {
            if (IsValidCampaignState(state.GetType()))
            {
                return CanSwitchTo(state as ICampaignState);
            }
            return false;
        }

        public abstract bool CanSwitchTo(ICampaignState state);

        public override string ToString()
        {
            return Name;
        }

        private bool IsValidCampaignState(Type type)
        {
            return type != null && typeof(ICampaignState).IsAssignableFrom(type);
        }

        private bool IsValidCampaignOperation(Type type)
        {
            return type != null && typeof(ICampaignOperation).IsAssignableFrom(type);
        }
    }
}