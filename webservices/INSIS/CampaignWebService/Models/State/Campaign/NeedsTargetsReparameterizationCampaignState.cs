﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class NeedsTargetsReParameterizationCampaignState : CampaignState
    {
        public override string Name => "NeedsTargetsReParameterization";

        public NeedsTargetsReParameterizationCampaignState() : base(8)
        {
        }

        public override bool NeedsTargetReParameterization() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.NeedsTargetReParameterization() || state.IsInReadyForApproval();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsTargetParameterization())
            {
                return new ReadyForApprovalCampaignState();
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}