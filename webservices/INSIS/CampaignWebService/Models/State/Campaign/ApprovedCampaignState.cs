﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class ApprovedCampaignState : CampaignState
    {
        public override string Name => "Approved";

        public ApprovedCampaignState() : base(1)
        {
        }

        public override bool IsInApproved() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.IsInApproved() || state.IsInScheduled();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsCampaignScheduled())
            {
                return justExecutedOperation.ResultingState();
            }

            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}