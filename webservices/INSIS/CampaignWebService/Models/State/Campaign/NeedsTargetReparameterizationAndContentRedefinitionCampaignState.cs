﻿using Isep.Insis.Helpers;

namespace Isep.Insis.Models.State
{
    public class NeedsTargetReParameterizationAndContentRedefinitionCampaignState : CampaignState
    {
        public NeedsTargetReParameterizationAndContentRedefinitionCampaignState() : base(9)
        {
        }

        public override string Name => "NeedsTargetReParameterizationAndContentRedefinition";

        public override bool NeedsContentRedefinitionAndTargetReParameterization() => true;

        public override bool CanSwitchTo(ICampaignState state)
        {
            return state.NeedsContentRedefinitionAndTargetReParameterization() || state.NeedsContentRedefinition() || state.NeedsTargetReParameterization();
        }

        public override ICampaignState NextStateAfter(ICampaignOperation justExecutedOperation)
        {
            if (justExecutedOperation.IsTargetParameterization())
            {
                return new NeedsContentsRedefinitionCampaignState();
            }
            if (justExecutedOperation.IsDefineContents())
            {
                return new NeedsTargetsReParameterizationCampaignState();
            }
            throw new InvalidCampaignOperationException(justExecutedOperation.ToString(), Name);
        }
    }
}