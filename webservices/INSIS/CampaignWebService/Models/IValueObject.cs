﻿using System;
using System.Runtime.Serialization;

namespace Isep.Insis.Models
{
    public interface IValueObject<TValueObject> : ISerializable, IEquatable<TValueObject>
        where TValueObject : IValueObject<TValueObject>
    {
    }
}