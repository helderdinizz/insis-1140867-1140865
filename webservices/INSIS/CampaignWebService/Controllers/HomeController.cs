﻿using Microsoft.AspNetCore.Mvc;

namespace Isep.Insis.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return new RedirectToRouteResult("GetAllCampaigns", new { });
        }
    }
}