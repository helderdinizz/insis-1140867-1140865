﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Isep.Insis.DTO;
using Isep.Insis.Extensions;
using Isep.Insis.Helpers;
using Isep.Insis.Models;
using Isep.Insis.Models.Campaign;
using Isep.Insis.Models.State;
using Isep.Insis.Repository;

using Microsoft.AspNetCore.Mvc;

namespace Isep.Insis.Controllers
{
    [Route("api/campaign")]
    public class CampaignController : Controller
    {
        private readonly ICampaignRepository _campaignRepository;
        private readonly IUserRepository _userRepository;

        public CampaignController(ICampaignRepository campaignRepository, IUserRepository userRepository)
        {
            this._campaignRepository = campaignRepository;
            this._userRepository = userRepository;
        }

        // GET api/campaign
        [HttpGet, Route("", Name = "GetAllCampaigns")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(IEnumerable<Insis.DTO.Campaign>))]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _campaignRepository.GetAll();
            return Ok(result.ToDTO());
        }

        // GET api/campaign/5
        [HttpGet("{id}", Name = "GetCampaign")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(IEnumerable<Insis.DTO.Campaign>))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAsync([FromRoute] Guid id)
        {
            var campaign = await _campaignRepository.GetAsync(id);
            if (campaign == null) return NotFound();
            return Ok(campaign.ToDTO());
        }

        [HttpGet("{id}/Content/Channel/{channel}")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.CampaignContent))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetContentsForCampaignInChannel([FromRoute] Guid id, [FromRoute] DispatchChannel channel)
        {
            var campaign = await _campaignRepository.GetAsync(id);

            if (campaign == null) return NotFound("Campaign not found.");

            var content = campaign.Contents
                    .Where(c => c.Channel == channel)
                    .FirstOrDefault();

            if (content != null)
            {
                return Ok(content);
            }
            throw new InvalidCampaignOperationException("No content was found for the specified channel");
        }

        [HttpGet("{id}/Content/Channel")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(IEnumerable<Insis.DTO.CampaignContent>))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetContentsForCampaign([FromRoute] Guid id)
        {
            var campaign = await _campaignRepository.GetAsync(id);

            if (campaign == null) return NotFound("Campaign not found.");

            var content = campaign.Contents
                    .Where(c => c.Selected);

            if (content != null)
            {
                return Ok(content);
            }
            throw new InvalidCampaignOperationException("No content was found for the specified channel");
        }

        [HttpGet("TimeZones/{id}")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK)]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.BadRequest)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetTimeZonesForCampaign([FromRoute] Guid id)
        {
            var campaign = await _campaignRepository.GetAsync(id);

            if (campaign == null) return NotFound("Campaign not found.");

            var users = await this._userRepository.GetAllForCampaign(campaign);

            var timezones = users.ToDTO(campaign.SendAtUtc)
                .GroupBy(u => u.TimeZone)
                .Select(g => g.First())
                .Select(u => new
                {
                    u.TimeZone,
                    u.Country,
                    u.SendAt
                });

            return Ok(new { Timezones = timezones });
        }

        [HttpGet("Targets/{id}/{country}/{channel}/{amount}")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.CampaignTarget))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.BadRequest)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetTargetsByCountryAndChannel([FromRoute] Guid id, [FromRoute]Country country, [FromRoute]DispatchChannel channel, [FromRoute]double amount)
        {
            if (country == Country.None) return Ok(Enumerable.Empty<Insis.DTO.CampaignTarget>());

            var campaign = await _campaignRepository.GetAsync(id);

            if (amount == default(double)) amount = int.MaxValue;

            if (campaign == null) return NotFound("Campaign not found.");

            if (campaign.State.IsInApproved() || campaign.State.IsInReadyForApproval())
            {
                if (!campaign.Contents.Any(content => content.Channel == channel))
                {
                    return BadRequest($"Campaign does not send to {channel.ToString()}");
                }

                var users = await this._userRepository.GetAllForCampaignAndChannel(campaign, channel);
                users = users.Where(u => u.Country == country).Take((int)amount);

                return Ok(users.ToDTO(campaign.SendAtUtc));
            }
            else
            {
                throw new InvalidCampaignOperationException("Campaign must be either approved or ready for approval in order to gather the target clients.");
            }
        }

        // POST api/campaign
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.Campaign))]
        [ProducesHttpResponseType(HttpStatusCode.BadRequest)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Post([FromBody]Insis.DTO.Campaign campaign)
        {
            var exists = await _campaignRepository.GetAsync(campaign.Id);
            if (exists == null)
            {
                var entityCampaign = campaign.ToModel();
                entityCampaign.Contents = new CampaignContentCatalog();
                entityCampaign.TargetClientParameters = new Insis.Models.TargetClientParameters();
                entityCampaign.State = new CreatedCampaignState();
                entityCampaign = await _campaignRepository.CreateAsync(entityCampaign);
                return CreatedAtRoute("GetCampaign", new { id = entityCampaign.Id }, entityCampaign.ToDTO());
            }
            else
            {
                throw new InvalidCampaignOperationException($"Campaign already created with the id {campaign.Id}");
            }
        }

        // PUT api/campaign/5
        [HttpPut("SetDefinition")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.Campaign))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> PutCampaignDefinition([FromBody] Insis.DTO.Campaign campaign)
        {
            var entityCampaign = await _campaignRepository.GetAsync(campaign.Id);
            if (entityCampaign != null)
            {
                if (entityCampaign.Contents == null)
                {
                    entityCampaign.Contents = campaign.Contents.ToModel();
                }
                else
                {
                    //TODO this could be a lot more efficient if the responsability was passed onto CampaignContentCatalog but cba, small project
                    campaign.Contents.ToList()
                            .ForEach(s => entityCampaign.Contents.AddCampaignContent(s.Channel, s.Content));
                    entityCampaign.Contents.ToList().RemoveAll(item => !campaign.Contents.ToList().Contains(item.ToDTO()));
                }

                var nextState = entityCampaign.State.NextStateAfter(typeof(DefinitionOfContentsCampaignOperation));
                entityCampaign.State = nextState as ICampaignState;

                entityCampaign = await _campaignRepository.UpdateAsync(entityCampaign);
                return Ok(entityCampaign.ToDTO());
            }
            else
            {
                throw new InvalidCampaignOperationException("Campaign not found.");
            }
        }

        [HttpPut("SetParameters")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.Campaign))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> PutCampaignParameterization([FromBody] Insis.DTO.Campaign campaign)
        {
            var entityCampaign = await _campaignRepository.GetAsync(campaign.Id);
            if (entityCampaign != null)
            {
                entityCampaign.TargetClientParameters = campaign.TargetClientParameters.ToModel();

                if (entityCampaign.Contents == null)
                {
                    entityCampaign.Contents = campaign.Contents.ToModel();
                }
                else
                {
                    campaign.Contents.ToList().ForEach(s => entityCampaign.Contents.SetSelectedContent(s.Channel, s.Selected));
                }

                var nextState = entityCampaign.State.NextStateAfter(typeof(TargetParameterizationCampaignOperation));
                entityCampaign.State = nextState as ICampaignState;

                entityCampaign = await _campaignRepository.UpdateAsync(entityCampaign);
                return Ok(entityCampaign.ToDTO());
            }
            else
            {
                throw new InvalidCampaignOperationException("Campaign not found.");
            }
        }

        [HttpPut("MarkAsSent/{id}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.Campaign))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> PutCampaignStateAsSent([FromRoute] Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Missing id for the specified request.");
            }

            var entityCampaign = await _campaignRepository.GetAsync(id);
            if (entityCampaign != null)
            {
                var nextState = entityCampaign.State.NextStateAfter(typeof(SendCampaignOperation));
                entityCampaign.State = nextState as ICampaignState;
                entityCampaign = await _campaignRepository.UpdateAsync(entityCampaign);
                return Ok(entityCampaign.ToDTO());
            }
            else
            {
                throw new InvalidCampaignOperationException("Campaign not found.");
            }
        }

        [HttpPut("ReviewCampaign/{id}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(Insis.DTO.Campaign))]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> ReviewCampaign([FromRoute] Guid id, [FromBody] CampaignReview campaignReview)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Missing id for the specified request.");
            }

            var entityCampaign = await _campaignRepository.GetAsync(id);
            if (entityCampaign != null)
            {
                entityCampaign.State = campaignReview.Decision.ResultingState();
                entityCampaign.SendAtUtc = campaignReview.ParsedSendAtUtc;

                entityCampaign = await _campaignRepository.UpdateAsync(entityCampaign);
                return Ok(entityCampaign.ToDTO());
            }
            else
            {
                throw new InvalidCampaignOperationException("Campaign not found.");
            }
        }

        // DELETE api/campaign/5
        [HttpDelete("{id}")]
        [ProducesHttpResponseType(HttpStatusCode.NoContent)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _campaignRepository.DeleteAsync(id);
            //return NoContent();
            return Ok(new { id = id });
        }
    }
}