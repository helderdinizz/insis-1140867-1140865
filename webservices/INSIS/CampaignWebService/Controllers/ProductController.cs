﻿using System;

using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Isep.Insis.Controllers
{
    [Route("products")]
    public class ProductController : Controller
    {
        [HttpGet]
        public IActionResult Index(Guid campaignId, Guid userId)
        {
            ViewBag.CampaignId = campaignId;
            ViewBag.UserId = userId;
            return View();
        }
    }
}
