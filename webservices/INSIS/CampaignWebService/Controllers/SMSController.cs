﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Isep.Insis.DTO;
using Isep.Insis.Helpers;
using Isep.Insis.Provider;
using Isep.Insis.Repository;

using Microsoft.AspNetCore.Mvc;

namespace Isep.Insis.Controllers
{
    [Route("/api/sms")]
    public class SMSController : Controller
    {
        private readonly ISmsProvider _provider;
        private readonly IUserRepository _userRepository;

        public SMSController(ISmsProvider provider, IUserRepository userRepository)
        {
            this._provider = provider;
            this._userRepository = userRepository;
        }

        [HttpPost, Route("send", Name = "SendSMS")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK, typeof(IEnumerable<Insis.DTO.Campaign>))]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Send([FromBody] SMSRequest request)
        {
            var user = await this._userRepository.GetAsync(request.UserId).ConfigureAwait(false);
            if (user != null)
            {
                await this._provider.SendAsync(user.PhoneNumber, request.Body).ConfigureAwait(false);
                return Ok();
            }
            return NotFound(new { Exception = "UserNotFoundException", Message = "User not found." });
        }
    }
}