﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Isep.Insis.Models;
using Isep.Insis.Repository;
using Isep.Insis.ViewModels;

using Microsoft.AspNetCore.Mvc;

namespace Isep.Insis.Controllers
{
    [Route("subscriptions")]
    public class SubscriptionController : Controller
    {
        private readonly ICampaignRepository _campaignRepository;
        private readonly IUserRepository _userRepository;

        public SubscriptionController(ICampaignRepository campaignRepository, IUserRepository userRepository)
        {
            this._campaignRepository = campaignRepository;
            this._userRepository = userRepository;
        }

        [HttpGet]
        [Route("", Name = "Manage")]
        public async Task<IActionResult> Manage(Guid userId)
        {
            var model = new ListSubscriptionsViewModel()
            {
                UserId = userId
            };

            var user = await this._userRepository.GetAsync(userId);

            if (user == null)
            {
                ModelState.AddModelError("UserNotFound", "User not found.");
                return View(model);
            }

            foreach (var dispatchChannel in (DispatchChannel[])Enum.GetValues(typeof(DispatchChannel)))
            {
                var channelViewModel = new SubscriptionViewModel
                {
                    Channel = dispatchChannel,
                    Checked = user.SubscribedChannels.Any(channel => channel == dispatchChannel)
                };
                model.Channels.Add(channelViewModel);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Manage([FromForm] ListSubscriptionsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this._userRepository.GetAsync(model.UserId);

                if (user == null)
                {
                    ModelState.AddModelError("UserNotFound", "User not found.");
                    return View(model);
                }

                user.SubscribedChannels = model.Channels.Where(vm => vm.Checked).Select(vm => vm.Channel).ToList();
                await this._userRepository.UpdateAsync(user);
                TempData["Success"] = "Updated successfully!";
            }
            return RedirectToRoute("Manage", new { userId = model.UserId });
        }
    }
}
