﻿using System;
using System.Net;
using EventSourcing.Core;
using EventSourcing.Core.Common;
using EventSourcing.Core.Common.Implementations;
using EventSourcing.Core.RabbitMQ;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

using Isep.Insis.Configuration;
using Isep.Insis.Events;
using Isep.Insis.Events.Handlers;
using Isep.Insis.Helpers;
using Isep.Insis.Provider;
using Isep.Insis.Repository;
using Isep.Insis.Repository.Mappings;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using NHibernate.Tool.hbm2ddl;
using RabbitMQ.Client;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

using Twilio;

namespace Isep.Campaign
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Verbose()
                        .Enrich.FromLogContext()
                        .WriteTo.Seq("http://isep-insis-seq.ukwest.cloudapp.azure.com")
                        .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var jsonSerializerDefaultSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };

            JsonConvert.DefaultSettings = () => jsonSerializerDefaultSettings;
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Formatting = jsonSerializerDefaultSettings.Formatting;
                    options.SerializerSettings.NullValueHandling = jsonSerializerDefaultSettings.NullValueHandling;
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                });

            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new Info { Title = "Campaign API", Version = "v1" });
                c.OrderActionsBy((apiDesc) => apiDesc.HttpMethod);
            });

            var persistenceConfig = Configuration.GetSection("PersistenceSettings").Get<PersistenceConfiguration>();

            var databaseConfiguration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(persistenceConfig.ConnectionString))
                .Mappings(configuration =>
                    configuration.FluentMappings
                        .AddFromAssemblyOf<CampaignMap>()
                ).BuildConfiguration();

            if (persistenceConfig.DropAndCreate)
            {
                var exporter = new SchemaUpdate(databaseConfiguration);
                exporter.Execute(true, true);
            }

            services.AddSingleton(databaseConfiguration.BuildSessionFactory());
            services.AddSingleton<ICampaignRepository, CampaignRepository>();
            services.AddSingleton<IUserRepository, InMemoryUserRepository>();

            var twilioConfig = Configuration.GetSection("Twilio").Get<TwilioConfiguration>();

            TwilioClient.Init(twilioConfig.AccountSID, twilioConfig.ApiKey);

            services.AddSingleton(twilioConfig);
            services.AddSingleton<ISmsProvider, TwilioSmsProvider>();

            services.AddRouting();

            //RegisterEventBus(services);
        }

        private void RegisterEventBus(IServiceCollection services)
        {
            services.AddSingleton<IEventBusSubscriptionManager, InMemoryEventBusSubscriptionManager>();

            services.AddTransient<ChannelBookedEventHandler>();

            services.AddSingleton<IRabbitConnection>(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<RabbitConnection>>();

                var eventBusConfiguration = Configuration.GetSection("EventBus");

                var factory = new ConnectionFactory()
                {
                    HostName = eventBusConfiguration["Hostname"]
                };

                if (!string.IsNullOrEmpty(eventBusConfiguration["Username"]))
                {
                    factory.UserName = eventBusConfiguration["Username"];
                }

                if (!string.IsNullOrEmpty(eventBusConfiguration["Password"]))
                {
                    factory.Password = eventBusConfiguration["Password"];
                }

                return new RabbitConnection(factory, logger);
            });

            services.AddSingleton<IEventBus, RabbitEventBus>(sp =>
            {
                var rabbitConnection = sp.GetRequiredService<IRabbitConnection>();
                var logger = sp.GetRequiredService<ILogger<RabbitEventBus>>();
                var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionManager>();

                return new RabbitEventBus(rabbitConnection, logger, Configuration, eventBusSubcriptionsManager, sp, "bookings");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            ConfigureCommon(app, env, loggerFactory);
            app.UseDeveloperExceptionPage();
        }

        private void ConfigureCommon(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware(typeof(ErrorHandling));

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddSerilog();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}");
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Campaign API V1");
            });
            //ConfigureEventBus(app);

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }

        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<ChannelBookedEvent, ChannelBookedEventHandler>();
        }
    }
}