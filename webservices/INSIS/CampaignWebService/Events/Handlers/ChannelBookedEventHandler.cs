﻿using EventSourcing.Core;
using EventSourcing.Core.Common;
using Isep.Insis.Models;
using Isep.Insis.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isep.Insis.Events.Handlers
{
    public class ChannelBookedEventHandler : IEventHandler<ChannelBookedEvent>
    {
        private readonly ILogger<ChannelBookedEventHandler> _logger;
        private readonly ICampaignRepository _campaignRepository;
        private static readonly ICampaignOperation Operation = new ScheduledCampaignOperation();

        public ChannelBookedEventHandler(ICampaignRepository campaignRepository, ILogger<ChannelBookedEventHandler> logger)
        {
            this._logger = logger;
            this._campaignRepository = campaignRepository;
        }

        public async Task Handle(ChannelBookedEvent @event)
        {
            _logger.LogDebug($"ChannelBookedEvent received: {@event.Id} - {@event.CampaignId}, occurred at {@event.OccurredAt}.");

            try
            {
                await HandleInternal(@event);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An exception has occurred while processing the event {@event.Id}.");
            }
        }

        private async Task HandleInternal(ChannelBookedEvent @event)
        {
            var campaign = await this._campaignRepository.GetAsync(@event.CampaignId);

            if (campaign != null)
            {
                var nextState = campaign.State.NextStateAfter(Operation);
                campaign.State = nextState;
                await this._campaignRepository.UpdateAsync(campaign);
            }
            else
            {
                throw new InvalidOperationException($"ChannelBookedEvent cannot be applied: {@event.Id} - {@event.CampaignId}, occurred at {@event.OccurredAt}. - No campaign exists with that id.");
            }
        }
    }
}
