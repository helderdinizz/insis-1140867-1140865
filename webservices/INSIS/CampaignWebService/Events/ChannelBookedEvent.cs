﻿using EventSourcing;
using EventSourcing.Core;
using System;
using System.Collections.Generic;

namespace Isep.Insis.Events
{
    public class ChannelBookedEvent : Event
    {
        public Guid CampaignId { get; set; }

        public IEnumerable<int> BookIds { get; set; }
    }
}
