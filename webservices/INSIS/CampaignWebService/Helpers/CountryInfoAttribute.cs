﻿using System;
using System.Globalization;
using System.Linq;

using NodaTime;
using NodaTime.TimeZones;

namespace Isep.Insis.Helpers
{
    //[AttributeUsage(AttributeTargets.Enum)]
    public class CountryInfoAttribute : Attribute
    {
        public string CountryCode { get; }

        public string TimeZoneName { get; }

        public CountryInfoAttribute(string timeZoneName)
        {
            this.TimeZoneName = timeZoneName?.Trim() ?? "Europe/Lisbon";
            this.CountryCode = TzdbDateTimeZoneSource.Default.ZoneLocations.SingleOrDefault(tz => tz.ZoneId.Equals(timeZoneName))?.CountryCode ?? "pt-PT";
        }

        public CultureInfo CultureInfo
        {
            get
            {
                return CultureInfo.GetCultureInfo(CountryCode);
            }
        }

        public DateTimeZone TimeZoneInfo
        {
            get
            {
                return DateTimeZoneProviders.Tzdb[TimeZoneName];
            }
        }

        public DateTime CurrentTime
        {
            get
            {
                return Instant.FromDateTimeUtc(DateTime.UtcNow).InZone(TimeZoneInfo).ToDateTimeUnspecified();
            }
        }
    }
}