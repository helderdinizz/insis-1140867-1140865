﻿using System;

namespace Isep.Insis.Helpers
{
    public class InvalidCampaignOperationException : Exception
    {
        public InvalidCampaignOperationException()
            : base("The performed operation is invalid in the campaign's current state.") { }

        public InvalidCampaignOperationException(string message)
            : base(message) { }

        public InvalidCampaignOperationException(string operation, string currentStateName)
            : base($"The performed operation {operation} is invalid in the campaign's current state {currentStateName}.") { }
    }
}