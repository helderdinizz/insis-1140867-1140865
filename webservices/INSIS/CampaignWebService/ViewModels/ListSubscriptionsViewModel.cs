﻿using System;
using System.Collections.Generic;

using Isep.Insis.Models;

namespace Isep.Insis.ViewModels
{
    public class ListSubscriptionsViewModel
    {
        public Guid UserId { get; set; }
        public IList<SubscriptionViewModel> Channels { get; set; } = new List<SubscriptionViewModel>();
    }

    public class SubscriptionViewModel
    {
        public DispatchChannel Channel { get; set; }
        public bool Checked { get; set; }
    }
}
