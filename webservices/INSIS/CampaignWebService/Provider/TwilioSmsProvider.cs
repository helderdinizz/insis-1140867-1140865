﻿using System.Threading.Tasks;

using Isep.Insis.Configuration;

using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Isep.Insis.Provider
{
    public class TwilioSmsProvider : ISmsProvider
    {
        private readonly TwilioConfiguration _configuration;

        public TwilioSmsProvider(TwilioConfiguration config)
        {
            this._configuration = config;
        }

        public async Task<bool> SendAsync(string phoneNumber, string body)
        {
            var to = new PhoneNumber(phoneNumber);
            var message = await MessageResource.CreateAsync(
                to,
                from: new PhoneNumber(this._configuration.FromPhoneNumber),
                body: body).ConfigureAwait(false);

            return true;
        }
    }
}