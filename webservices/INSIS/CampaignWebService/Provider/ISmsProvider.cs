﻿using System.Threading.Tasks;

namespace Isep.Insis.Provider
{
    public interface ISmsProvider
    {
        Task<bool> SendAsync(string phoneNumber, string body);
    }
}