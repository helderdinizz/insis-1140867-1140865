﻿using System.Collections.Generic;

namespace Isep.Insis.DTO
{
    public class TargetClientParameters
    {
        public IEnumerable<string> Countries { get; set; }

        public int MinAge { get; set; }
        public int MaxAge { get; set; }
    }
}