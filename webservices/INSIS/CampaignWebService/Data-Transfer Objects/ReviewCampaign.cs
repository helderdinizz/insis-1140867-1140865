﻿using Isep.Insis.Helpers;
using Isep.Insis.Models.State;

namespace Isep.Insis.DTO
{
    public enum ReviewCampaignDecision
    {
        Approve,
        NeedsTargetReParameterization,
        NeedsContentRedefinition,
        NeedsTargetReParameterizationAndContentRedefinition
    }

    public static class ReviewCampaignDecisionExtensions
    {
        public static ICampaignState ResultingState(this ReviewCampaignDecision decision)
        {
            ICampaignState resultingState;
            switch (decision)
            {
                case ReviewCampaignDecision.Approve:
                    resultingState = new ApprovedCampaignState();
                    break;

                case ReviewCampaignDecision.NeedsTargetReParameterization:
                    resultingState = new NeedsTargetsReParameterizationCampaignState();
                    break;

                case ReviewCampaignDecision.NeedsContentRedefinition:
                    resultingState = new NeedsContentsRedefinitionCampaignState();
                    break;

                case ReviewCampaignDecision.NeedsTargetReParameterizationAndContentRedefinition:
                    resultingState = new NeedsTargetReParameterizationAndContentRedefinitionCampaignState();
                    break;

                default:
                    throw new InvalidCampaignOperationException($"No valid state for the review campaign decision {decision.ToString()}.");
            }
            return resultingState;
        }
    }
}