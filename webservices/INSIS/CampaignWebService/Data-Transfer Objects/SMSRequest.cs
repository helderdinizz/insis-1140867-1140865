﻿using System;

namespace Isep.Insis.DTO
{
    public class SMSRequest
    {
        public Guid UserId { get; set; }
        public string Body { get; set; }
    }
}