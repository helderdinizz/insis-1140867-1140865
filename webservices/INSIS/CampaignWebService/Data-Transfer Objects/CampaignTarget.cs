﻿using System;
using System.Collections.Generic;

using Isep.Insis.Models;

namespace Isep.Insis.DTO
{
    public class CampaignTarget
    {
        public Guid Id { get; internal set; }

        public string Country { get; internal set; }

        public int Age { get; internal set; }

        public string Email { get; internal set; }

        public string Username { get; internal set; }

        public string LocalTime { get; internal set; }

        public string SendAt { get; internal set; }

        public string TimeZone { get; internal set; }

        public string PhoneNumber { get; internal set; }

        public IEnumerable<DispatchChannel> SubscribedChannels { get; internal set; }
    }
}