﻿using Isep.Insis.Models;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Isep.Insis.DTO
{
    public class CampaignContent
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public DispatchChannel Channel { get; set; }

        public string Content { get; set; }

        public bool Selected { get; set; }
    }
}