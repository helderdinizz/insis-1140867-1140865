﻿using System;
using System.Collections.Generic;
using System.Globalization;

using Newtonsoft.Json;

namespace Isep.Insis.DTO
{
    public class Campaign
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string Objective { get; set; }

        public IEnumerable<CampaignContent> Contents { get; set; }

        public TargetClientParameters TargetClientParameters { get; set; }

        public string State { get; set; }

        public string SendAtUtc { get; set; }

        [JsonIgnore]
        public DateTime ParsedSendAtUtc => DateTime.TryParseExact(SendAtUtc, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTime) ? dateTime : default(DateTime);
    }
}