﻿using System;
using System.Globalization;

using Newtonsoft.Json;

namespace Isep.Insis.DTO
{
    public class CampaignReview
    {
        public ReviewCampaignDecision Decision { get; set; }

        public string SendAtUtc { get; set; }

        [JsonIgnore]
        public DateTime ParsedSendAtUtc => DateTime.TryParseExact(SendAtUtc, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTime) ? dateTime : default(DateTime);
    }
}