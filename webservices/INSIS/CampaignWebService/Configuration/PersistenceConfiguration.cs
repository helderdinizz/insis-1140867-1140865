﻿namespace Isep.Insis.Configuration
{
    public sealed class PersistenceConfiguration
    {
        public string ConnectionString { get; set; }

        public bool DropAndCreate { get; set; }
    }
}