﻿namespace Isep.Insis.Configuration
{
    public class TwilioConfiguration
    {
        public string AccountSID { get; set; }
        public string ApiKey { get; set; }

        public string FromPhoneNumber { get; set; }
    }
}