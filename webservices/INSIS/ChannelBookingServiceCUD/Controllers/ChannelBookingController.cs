﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ChannelBookingService.DTO;
using ChannelBookingService.Gateway;
using ChannelBookingService.Helpers;
using EventSourcing.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ChannelBookingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChannelBookingController : ControllerBase
    {
        // GET api/values
        private readonly IEventBus _eventBus;
        private readonly ICampaignGateway _campaignGateway;
        private readonly ILogger<ChannelBookingController> _logger;

        public ChannelBookingController(
            IEventBus eventBus,
            ICampaignGateway campaignGateway,
            ILogger<ChannelBookingController> logger)
        {
            this._eventBus = eventBus;
            this._campaignGateway = campaignGateway;
            this._logger = logger;
        }

        [HttpPost, Route("Book", Name = "BookChannel")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK)]
        [ProducesHttpResponseType(HttpStatusCode.BadRequest)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> BookChannel([FromBody]ChannelBookingRequest payload)
        {
            if (payload is null)
            {
                _logger.LogDebug("Payload is null.");
                throw new StatusCodeException("Payload sent is null.", HttpStatusCode.BadRequest);
            }

            var campaignExists = await this._campaignGateway.ExistsAsync(payload.CampaignID);
            if (!campaignExists)
            {
                _logger.LogDebug("Campaign {CampaignId} does not exist.", payload.CampaignID);
                throw new StatusCodeException($"Requested campaign ({payload.CampaignID}) does not exist.", HttpStatusCode.BadRequest);
            }

            foreach (var channel in payload.DispatchChannels)
            {
                var available = await this._eventBus.GetAvailabilityFor(channel, payload.StartTime, payload.EndDate);
                if (!available)
                {
                    _logger.LogDebug("No availabilty for requested booking {BookRequest}.", payload);
                    throw new StatusCodeException($"Requested booking overlaps with another one for channel {channel}.", HttpStatusCode.BadRequest);
                }
            }

            var channelBooking = await this._eventBus.CreateManyAsync(payload.DispatchChannels.Select(channel => new ChannelBookingService.Models.ChannelBooking()
            {
                CampaignID = payload.CampaignID,
                DispatchChannel = channel,
                StartTime = payload.StartTime,
                EndTime = payload.EndDate
            }));

            _logger.LogDebug("Booked channels for request {BookRequest}.", payload);

            return Ok(channelBooking);
        }
    }
}
