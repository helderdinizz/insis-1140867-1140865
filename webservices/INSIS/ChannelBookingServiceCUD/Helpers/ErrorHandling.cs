﻿using System;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace ChannelBookingService.Helpers
{
    public class ErrorHandling
    {
        private readonly RequestDelegate _next;

        public ErrorHandling(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context, IHostingEnvironment env)
        {
            try
            {
                await this._next(context).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, env).ConfigureAwait(false);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception exception, IHostingEnvironment env)
        {
            var code = HttpStatusCode.InternalServerError;
            string result = null;

            if (exception is StatusCodeException statusCodeException)
            {
                code = statusCodeException.StatusCode;
            }

            result = JsonConvert.SerializeObject(new
            {
                Exception = exception.GetType().Name,
                exception.Message,
                StackTrace = env.IsDevelopment() ? exception.StackTrace : null
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            await context.Response.WriteAsync(result).ConfigureAwait(false);
        }
    }
}