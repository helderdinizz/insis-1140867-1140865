﻿using System;
using System.Net;

using Microsoft.AspNetCore.Mvc;

namespace ChannelBookingService.Helpers
{
    public class ProducesHttpResponseTypeAttribute : ProducesResponseTypeAttribute
    {
        public ProducesHttpResponseTypeAttribute(HttpStatusCode statusCode) : base((int)statusCode)
        {
        }

        public ProducesHttpResponseTypeAttribute(HttpStatusCode statusCode, Type type) : base(type, (int)statusCode)
        {
        }
    }
}