﻿using System.Linq;
using System.Security.Claims;

using IdentityModel;

using IdentityServer4.Test;

using IdentityService.Models;

namespace IdentityService.Infrastructure.Helpers
{
    public static class TestUserExtensions
    {

        public static User ToEntity(this TestUser testUser)
        {
            if (testUser == null)
            {
                return null;
            }

            return new User
            {
                Id = testUser.SubjectId,
                Active = testUser.IsActive,
                Email = testUser.Claims.FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.Email)).Value,
                Password = testUser.Password.ToSha256()
            };
        }
    }
}
