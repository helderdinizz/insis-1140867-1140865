﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using IdentityServer4.Test;

using IdentityService.Infrastructure.Helpers;

namespace IdentityService.Infrastructure.Seed
{
    public static class Seeder
    {
        private static readonly Lazy<IEnumerable<TestUser>> _Users = new Lazy<IEnumerable<TestUser>>(GetTestUsers);
        private static readonly Lazy<IEnumerable<Client>> _Clients = new Lazy<IEnumerable<Client>>(GetTestClients);
        private static readonly Lazy<IEnumerable<ApiResource>> _ApiResources = new Lazy<IEnumerable<ApiResource>>(GetTestApiResources);

        private static IEnumerable<TestUser> GetTestUsers()
        {
            return new List<TestUser>()
            {
                new TestUser {
                    SubjectId = Guid.NewGuid().ToString(),
                    IsActive = true,
                    Username = "andre_silva",
                    Password = "xpto",
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, "1140865@isep.ipp.pt"),
                        new Claim(ClaimTypes.GivenName, "André Silva"),
                        new Claim(ClaimTypes.Role, "admin")
                    }
                },
                new TestUser {
                    SubjectId = Guid.NewGuid().ToString(),
                    IsActive = true,
                    Username = "helder_diniz",
                    Password = "xpto",
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, "1140867@isep.ipp.pt"),
                        new Claim(ClaimTypes.GivenName, "Hélder Diniz"),
                        new Claim(ClaimTypes.Role, "admin")
                    }
                },
                new TestUser{
                    SubjectId = Guid.NewGuid().ToString(),
                    IsActive = false,
                    Username = "nelson_semedo",
                    Password = "xpto",
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, "1140865@isep.ipp.pt"),
                        new Claim(ClaimTypes.GivenName, "Nélson Semedo"),
                        new Claim(ClaimTypes.Role, "admin")
                    }
                },
                new TestUser{
                    SubjectId = Guid.NewGuid().ToString(),
                    IsActive = true,
                    Username = "cristiano_ronaldo",
                    Password = "xpto",
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, "1140865@isep.ipp.pt"),
                        new Claim(ClaimTypes.GivenName, "Cristiano Ronaldo"),
                        new Claim(ClaimTypes.Role, "admin")
                    }
                },
                new TestUser{
                    SubjectId = Guid.NewGuid().ToString(),
                    IsActive = true,
                    Username = "jose_fonte",
                    Password = "xpto",
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, "1140865@isep.ipp.pt"),
                        new Claim(ClaimTypes.GivenName, "José Fonte"),
                        new Claim(ClaimTypes.Role, "admin")
                    }
                },
                new TestUser{
                    SubjectId = Guid.NewGuid().ToString(),
                    IsActive = true,
                    Username = "eder_lopes",
                    Password = "xpto",
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, "1140867@isep.ipp.pt"),
                        new Claim(ClaimTypes.GivenName, "Éderzito Lopes"),
                        new Claim(ClaimTypes.Role, "admin")
                    }
                }
            }.AsReadOnly();
        }

        private static IEnumerable<Client> GetTestClients()
        {
            return new List<Client>
            {
                new Client
                    {
                        ClientId = "client",
                        AllowedGrantTypes = new List<string>(){
                            GrantType.ResourceOwnerPassword, GrantType.ClientCredentials
                        },
                        ClientSecrets =
                        {
                            new Secret("campaign_secret".Sha256())
                        },
                        AllowedScopes = { "campaign_api" }
                    }
            }.AsReadOnly();
        }

        private static IEnumerable<ApiResource> GetTestApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("campaign_api", "Campaign API")
            }.AsReadOnly();
        }

        public static IEnumerable<TestUser> Users
        {
            get
            {
                return _Users.Value;
            }
        }

        public static IEnumerable<Client> Clients
        {
            get
            {
                return _Clients.Value;
            }
        }

        public static IEnumerable<ApiResource> ApiResources
        {
            get
            {
                return _ApiResources.Value;
            }
        }

        internal static void Seed(ConfigurationDbContext context, bool deletePrevious)
        {
            if (deletePrevious)
            {
                context.RemoveRange(context.Clients.ToList());
                context.SaveChanges();
            }

            foreach (var client in Clients)
            {
                context.Clients.Add(client.ToEntity());
            }
            context.SaveChanges();

            if (deletePrevious)
            {
                context.RemoveRange(context.ApiResources.ToList());
                context.SaveChanges();
            }

            foreach (var resource in ApiResources)
            {
                context.ApiResources.Add(resource.ToEntity());
            }
            context.SaveChanges();
        }

        internal static void Seed(IdentityContext context, bool deletePrevious)
        {
            if (deletePrevious)
            {
                context.RemoveRange(context.Users.ToList());
                context.SaveChanges();
            }

            foreach (var user in Users)
            {
                context.Users.Add(user.ToEntity());
            }
            context.SaveChanges();
        }
    }
}