﻿using System.Threading.Tasks;

using IdentityServer4.Models;
using IdentityServer4.Validation;

namespace IdentityService.Infrastructure.Authentication
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private IAuthenticationRepository _authenticationRepository;

        public ResourceOwnerPasswordValidator(IAuthenticationRepository rep)
        {
            this._authenticationRepository = rep;
        }

        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (_authenticationRepository.ValidatePassword(context.UserName, context.Password))
            {
                context.Result = new GrantValidationResult(_authenticationRepository.GetUserByUsername(context.UserName).Id, "password", null, "local", null);
                return Task.FromResult(context.Result);
            }
            context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid credentials supplied.", null);
            return Task.FromResult(context.Result);
        }
    }
}
