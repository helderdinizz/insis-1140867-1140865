﻿using System.Linq;

using IdentityModel;

using IdentityService.Models;

namespace IdentityService.Infrastructure.Authentication
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private IdentityContext _context;

        public AuthenticationRepository(IdentityContext context)
        {
            _context = context;
        }

        public User GetUserById(string id)
        {
            var user = _context.Users.Where(u => u.Id == id).FirstOrDefault();
            return user;
        }

        public User GetUserByUsername(string username)
        {
            var user = _context.Users.Where(u => string.Equals(u.Email, username)).FirstOrDefault();
            return user;
        }


        public bool ValidatePassword(string username, string plainTextPassword)
        {
            var user = _context.Users.Where(u => string.Equals(u.Email, username)).FirstOrDefault();
            if (user == null) return false;
            if (string.Equals(plainTextPassword.ToSha256(), user.Password)) return true;
            return false;
        }
    }
}
