﻿using IdentityService.Models;

namespace IdentityService.Infrastructure.Authentication
{
    public interface IAuthenticationRepository
    {
        User GetUserById(string id);
        User GetUserByUsername(string username);
        bool ValidatePassword(string username, string plainTextPassword);
    }
}
