﻿using System.Linq;
using System.Reflection;

using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.Services;
using IdentityServer4.Validation;

using IdentityService.Infrastructure.Authentication;
using IdentityService.Infrastructure.Seed;
using IdentityService.Models;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityService
{
    public partial class Startup
    {

        private IConfigurationSection IdentityConfiguration => Configuration.GetSection("Identity");

        private void ConfigureIdentityServerServices(IServiceCollection services)
        {

            services.AddDistributedMemoryCache();

            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            services.AddTransient<IProfileService, ProfileService>();
            services.AddTransient<IAuthenticationRepository, AuthenticationRepository>();

            services.AddDbContext<IdentityContext>(options =>
                            options.UseSqlServer(PersistenceConfiguration["ConnectionString"])
                    );

            string migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddConfigurationStore(config =>
                {
                    config.ConfigureDbContext = builder =>
                        builder.UseSqlServer(PersistenceConfiguration["ConnectionString"], sqlOpt => sqlOpt.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(config =>
                {
                    config.ConfigureDbContext = builder =>
                        builder.UseSqlServer(PersistenceConfiguration["ConnectionString"], sqlOpt => sqlOpt.MigrationsAssembly(migrationsAssembly));
                    config.EnableTokenCleanup = true;
                    config.TokenCleanupInterval = IdentityConfiguration.GetValue<int>("TokenCleanupInterval");
                });

            services.AddIdentity<User, IdentityRole>()
        .AddEntityFrameworkStores<IdentityContext>()
        .AddDefaultTokenProviders();

            //var schemeName = "oidc";
            //var provider = services.BuildServiceProvider();
            //var dataProtectionProvider = provider.GetDataProtectionProvider();
            //var distributedCache = provider.GetRequiredService<IDistributedCache>();

            //var dataProtector = dataProtectionProvider.CreateProtector(
            //    "OpenIdConnect",
            //    typeof(string).FullName, schemeName,
            //    "v1");

            //var dataFormat = new CachedPropertiesDataFormat(distributedCache, dataProtector);
            //var tenantId = "7465e2df-3234-4579-91b0-ff24bec9f70e";

            //services.AddAuthentication()
            //    .AddOpenIdConnect("oidc", "Azure AD", options =>
            //    {
            //        options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
            //        options.ClientId = "dd5e007a-317b-436f-9fd4-b63bde788f7e";
            //        options.Authority = $"https://login.windows.net/{tenantId}";
            //        options.ResponseType = OpenIdConnectResponseType.IdToken;
            //        options.StateDataFormat = dataFormat;
            //        options.ClientSecret = "pmxIJAujcoQ6wX+I9ryQ45hvia2N5308QosfV/TPIkU=";
            //    });
        }

        private void ConfigureIdentityServerServicesDevelopment(IServiceCollection services)
        {
            services
                .AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryClients(Seeder.Clients)
                .AddTestUsers(Seeder.Users.ToList())
                .AddInMemoryApiResources(Seeder.ApiResources);
        }

        private void ConfigureIdentityServer(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (PersistenceConfiguration.GetValue<bool>("SeedData"))
            {
                Seeder.Seed(app.ApplicationServices.GetRequiredService<ConfigurationDbContext>(), true);
                Seeder.Seed(app.ApplicationServices.GetRequiredService<IdentityContext>(), true);
            }

            app.UseIdentityServer();

        }
    }
}