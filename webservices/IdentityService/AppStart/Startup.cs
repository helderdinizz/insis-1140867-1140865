﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityService
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }
        private IConfigurationSection ApplicationInfoConfiguration => Configuration.GetSection("ApplicationInfo");
        private IConfigurationSection PersistenceConfiguration => Configuration.GetSection("PersistenceConfiguration");

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureCommonServices(services);
            ConfigureIdentityServerServices(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ConfigureCommon(app, env);
            ConfigureIdentityServer(app, env);
        }

        #region Common

        private void ConfigureCommonServices(IServiceCollection services)
        {
            services.AddMvc();

            ConfigureSwagger(services);
        }

        private void ConfigureCommon(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();
            app.UseMvc();

            ConfigureSwagger(app, env);
        }

        #endregion Common

        #region Development

        public void ConfigureDevelopmentServices(IServiceCollection services)
        {
            ConfigureCommonServices(services);
            ConfigureIdentityServerServicesDevelopment(services);
        }

        public void ConfigureDevelopment(IApplicationBuilder app, IHostingEnvironment env)
        {
            ConfigureCommon(app, env);
            ConfigureIdentityServer(app, env);
        }

        #endregion Development
    }
}