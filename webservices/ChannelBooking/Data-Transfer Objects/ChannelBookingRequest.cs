﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ChannelBookingService.Models;

using Newtonsoft.Json;

namespace ChannelBookingService.DTO
{
    public class ChannelBookingRequest
    {
        [Required]
        public Guid CampaignID { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public int DurationMinutes { get; set; }

        [JsonIgnore]
        internal DateTime EndDate
        {
            get
            {
                return this.StartTime + TimeSpan.FromMinutes(this.DurationMinutes);
            }
        }

        [Required]
        public IEnumerable<DispatchChannel> DispatchChannels { get; set; }

    }
}
