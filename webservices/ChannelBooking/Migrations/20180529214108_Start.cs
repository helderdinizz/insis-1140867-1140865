﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ChannelBookingService.Migrations
{
    public partial class Start : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateSequence(
                name: "EntityFrameworkHiLoSequence",
                schema: "public",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "ChannelBookings",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    campaign = table.Column<int>(nullable: false),
                    channel = table.Column<int>(nullable: false),
                    end_time = table.Column<DateTime>(nullable: false),
                    start_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChannelBookings", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChannelBookings",
                schema: "public");

            migrationBuilder.DropSequence(
                name: "EntityFrameworkHiLoSequence",
                schema: "public");
        }
    }
}
