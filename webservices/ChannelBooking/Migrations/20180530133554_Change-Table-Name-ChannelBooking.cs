﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ChannelBookingService.Migrations
{
    public partial class ChangeTableNameChannelBooking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ChannelBookings",
                schema: "public",
                table: "ChannelBookings");

            migrationBuilder.RenameTable(
                name: "ChannelBookings",
                schema: "public",
                newName: "channel_booking");

            migrationBuilder.AddPrimaryKey(
                name: "PK_channel_booking",
                schema: "public",
                table: "channel_booking",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_channel_booking",
                schema: "public",
                table: "channel_booking");

            migrationBuilder.RenameTable(
                name: "channel_booking",
                schema: "public",
                newName: "ChannelBookings");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ChannelBookings",
                schema: "public",
                table: "ChannelBookings",
                column: "id");
        }
    }
}
