﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChannelBookingService.Migrations
{
    public partial class ChangeCampaignIdToGuidType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.DropTable("channel_booking", "public");
            migrationBuilder.CreateTable(
                name: "channel_booking",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    campaign = table.Column<Guid>(nullable: false),
                    channel = table.Column<int>(nullable: false),
                    end_time = table.Column<DateTime>(nullable: false),
                    start_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChannelBookings", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
