﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;

using ChannelBookingService.DTO;
using ChannelBookingService.Events;
using ChannelBookingService.Gateway;
using ChannelBookingService.Helpers;
using ChannelBookingService.Repository;
using EventSourcing.Core;
using EventSourcing.Core.Common;
using EventSourcing.Store.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ChannelBooking.Controllers
{
    [Route("api/[controller]")]
    public class ChannelBookingController : ControllerBase
    {
        private readonly IChannelBookingRepository _bookingRepository;
        private readonly ICampaignGateway _campaignGateway;
        private readonly IEventStoreService _eventStoreService;
        private readonly ILogger<ChannelBookingController> _logger;

        public ChannelBookingController(
            IChannelBookingRepository repository, 
            ICampaignGateway campaignGateway,
            IEventStoreService eventStoreService,
            ILogger<ChannelBookingController> logger)
        {
            this._bookingRepository = repository;
            this._campaignGateway = campaignGateway;
            this._eventStoreService = eventStoreService;
            this._logger = logger;
        }

        [HttpGet, Route("", Name = "GetBookings")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetBookings()
        {
            var bookings = await this._bookingRepository.GetAll();

            return Ok(bookings);
        }

        [HttpGet, Route("{id:int}", Name = "GetBooking")]
        [ProducesHttpResponseType(HttpStatusCode.OK)]
        [ProducesHttpResponseType(HttpStatusCode.NotFound)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetBooking(int id)
        {
            var booking = await this._bookingRepository.GetAsync(id);

            if (booking is null)
            {
                return NotFound();
            }

            return Ok(booking);
        }
    }
}
