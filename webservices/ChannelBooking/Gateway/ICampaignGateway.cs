﻿using System;
using System.Threading.Tasks;

namespace ChannelBookingService.Gateway
{
    public interface ICampaignGateway
    {
        Task<bool> ExistsAsync(Guid campaignId);
    }
}
