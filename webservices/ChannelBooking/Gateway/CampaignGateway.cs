﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

using ChannelBookingService.Helpers;

using Microsoft.Extensions.Configuration;

namespace ChannelBookingService.Gateway
{
    public class CampaignGateway : ICampaignGateway
    {
        private readonly IConfiguration _configuration;

        public CampaignGateway(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public async Task<bool> ExistsAsync(Guid campaignId)
        {
            using (var handler = new HttpClientHandler())
            {
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                using (var client = new HttpClient(handler)
                {
                    BaseAddress = this._configuration.GetSection("Endpoints").GetValue<Uri>("CampaignAPI")
                })
                {
                    try
                    {
                        var response = await client.GetAsync($"/api/campaign/{campaignId}");
                        return response.IsSuccessStatusCode;
                    }
                    catch (Exception ex)
                    {
                        throw new StatusCodeException("An error occurred.", ex, System.Net.HttpStatusCode.InternalServerError);
                    }
                }
            }
        }
    }
}
