﻿using EventSourcing;
using EventSourcing.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelBookingService.Events
{
    public class ChannelBookedRequestResultEvent : Event
    {
        public string Success { get; set; }
        public Guid CampaignId { get; set; }
        public IEnumerable<int> BookIds { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }
    }
}
