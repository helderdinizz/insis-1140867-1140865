﻿using ChannelBookingService.Repository;
using EventSourcing.Core.Common;
using EventSourcing.Store.Service;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelBookingService.Events.Handlers
{
    public class ChannelBookRequestedEventHandler : IEventHandler<ChannelBookRequestedEvent>
    {

        private readonly IChannelBookingRepository _repository;
        private readonly IEventStoreService _eventstore;
        private readonly ILogger<ChannelBookRequestedEventHandler> _logger;

        public ChannelBookRequestedEventHandler(
            IChannelBookingRepository repository, 
            IEventStoreService eventstore,
            ILogger<ChannelBookRequestedEventHandler> logger)
        {
            this._repository = repository;
            this._eventstore = eventstore;
            this._logger = logger;
        }

        public async Task Handle(ChannelBookRequestedEvent @event)
        {
            foreach (var channel in @event.DispatchChannels)
            {
                var available = await this._repository.GetAvailabilityFor(channel, @event.StartTime, @event.StartTime + TimeSpan.FromMinutes(@event.DurationMinutes));
                if (!available)
                {
                    _logger.LogDebug("No availabilty for requested booking {BookRequest}.", @event.RequestId);
                    await this._eventstore.SaveEventAsync(new ChannelBookedRequestResultEvent
                    {
                        Success = "Failed"
                    });
                }
            }

            var channelBooking = await this._repository.CreateManyAsync(@event.DispatchChannels.Select(channel => new ChannelBookingService.Models.ChannelBooking()
            {
                CampaignID = @event.CampaignId,
                DispatchChannel = channel,
                StartTime = @event.StartTime,
                EndTime = @event.StartTime + TimeSpan.FromMinutes(@event.DurationMinutes)
            }));

            _logger.LogDebug("Booked channels for request {BookRequest}.", @event.RequestId);

            await this._eventstore.SaveEventAsync(new ChannelBookedRequestResultEvent
            {
                BookIds = channelBooking.Select(x => x.ChannelBookingId),
                CampaignId = channelBooking.Select(x => x.CampaignID).FirstOrDefault(),
                StartTime = channelBooking.Select(x => x.StartTime).FirstOrDefault(),
                EndTime = channelBooking.Select(x => x.EndTime).FirstOrDefault(),
                Success = "Completed"
            });
        }
    }
}
