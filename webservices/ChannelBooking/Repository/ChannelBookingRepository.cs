﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ChannelBookingService.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace ChannelBookingService.Repository
{
    public class ChannelBookingRepository : GenericRepository<Models.ChannelBooking, int>, IChannelBookingRepository
    {
        public ChannelBookingRepository(DataContext context) : base(context)
        {
        }

        public async override Task<IEnumerable<Models.ChannelBooking>> GetAll()
        {
            return await _context.ChannelBookings.ToListAsync();
        }

        public override async Task<Models.ChannelBooking> GetAsync(int id)
        {
            return await _context.ChannelBookings.Where(booking => booking.ChannelBookingId == id).FirstOrDefaultAsync();
        }

        public async Task<bool> GetAvailabilityFor(DispatchChannel channel, DateTime startDate, DateTime endDate)
        {
            var overlappingBooking = await _context.ChannelBookings
                .Where(booking => booking.IsBetween(startDate, endDate)
                       && booking.DispatchChannel == channel).FirstOrDefaultAsync();
            return overlappingBooking is null;
        }
    }
}
