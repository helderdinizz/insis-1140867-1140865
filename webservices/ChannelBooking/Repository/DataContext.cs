﻿
using Microsoft.EntityFrameworkCore;

namespace ChannelBookingService.Repository
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Models.ChannelBooking> ChannelBookings { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("public");

            var channelBookingModelBuilder = builder.Entity<Models.ChannelBooking>();

            channelBookingModelBuilder.ToTable("channel_booking");

            channelBookingModelBuilder.HasKey(m => m.ChannelBookingId);
            channelBookingModelBuilder.Property(m => m.ChannelBookingId).ForNpgsqlUseSequenceHiLo().HasColumnName("id");
            channelBookingModelBuilder.Property(m => m.StartTime).HasColumnName("start_time");
            channelBookingModelBuilder.Property(m => m.EndTime).HasColumnName("end_time");
            channelBookingModelBuilder.Property(m => m.DispatchChannel).HasColumnName("channel");
            channelBookingModelBuilder.Property(m => m.CampaignID).HasColumnName("campaign");

            base.OnModelCreating(builder);
        }
    }
}
