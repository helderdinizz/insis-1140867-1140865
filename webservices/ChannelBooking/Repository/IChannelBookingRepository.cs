﻿using System;
using System.Threading.Tasks;

using ChannelBookingService.Models;

namespace ChannelBookingService.Repository
{
    public interface IChannelBookingRepository : IRepository<Models.ChannelBooking, int>
    {
        Task<bool> GetAvailabilityFor(DispatchChannel channel, DateTime startDate, DateTime endTime);
    }
}
