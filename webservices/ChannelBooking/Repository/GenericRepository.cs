﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelBookingService.Repository
{
    public abstract class GenericRepository<TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : class
    {
        protected readonly DataContext _context;

        protected GenericRepository(DataContext context)
        {
            this._context = context;
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<IEnumerable<TEntity>> CreateManyAsync(IEnumerable<TEntity> entities)
        {
            var results = new List<TEntity>();
            var strategy = _context.Database.CreateExecutionStrategy();
            await strategy.ExecuteAsync(async () =>
            {
                using (var transaction = _context.Database.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        await this.CreateAsync(entity);
                        results.Add(entity);
                    }
                    transaction.Commit();
                }
            });

            return results;
        }

        public virtual async Task DeleteAsync(TId id)
        {
            var entity = await GetAsync(id);
            if (entity == null)
            {
                throw new InvalidOperationException($"Entity with id {id} not found.");
            }
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public abstract Task<IEnumerable<TEntity>> GetAll();

        public abstract Task<TEntity> GetAsync(TId id);

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            _context.Update(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public virtual void Dispose() => GC.SuppressFinalize(this);
    }
}