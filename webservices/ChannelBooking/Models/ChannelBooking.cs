﻿using System;

namespace ChannelBookingService.Models
{
    public class ChannelBooking
    {
        public int ChannelBookingId { get; set; }

        public Guid CampaignID { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DispatchChannel DispatchChannel { get; set; }

        public bool IsBetween(DateTime startDate, DateTime endDate)
        {
            return this.StartTime <= endDate && startDate <= this.EndTime;
        }

    }
}
