﻿namespace ChannelBookingService.Models
{
    public enum DispatchChannel
    {
        Email, SMS, SocialNetwork
    }
}
