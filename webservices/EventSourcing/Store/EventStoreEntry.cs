﻿using EventSourcing.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store
{
    public sealed class EventStoreEntry
    {
        private EventStoreEntry() { }
        public EventStoreEntry(Event @event)
        {
            EventId = @event.Id;
            CreationTime = @event.OccurredAt;
            EventTypeName = @event.GetType().FullName;
            Content = JsonConvert.SerializeObject(@event);
            State = EventStoreEntryState.NotPublished;
            TimesSent = 0;
        }

        public Guid EventId { get; set; }
        public string EventTypeName { get; set; }
        public EventStoreEntryState State { get; set; }
        public int TimesSent { get; set; }
        public DateTime CreationTime { get; set; }
        public string Content { get; set; }
    }
}
