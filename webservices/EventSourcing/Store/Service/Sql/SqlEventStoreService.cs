﻿using EventSourcing.Core;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store.Service.Sql
{
    public class SqlEventStoreService : IResilientEventStoreService
    {
        private readonly EventStoreContext _context;

        public SqlEventStoreService(EventStoreContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Event>> GetNotPublishedEventsAsync()
        {
            Expression<Func<EventStoreEntry, bool>> filter = (eventStoreEntry) => eventStoreEntry.State == EventStoreEntryState.NotPublished || eventStoreEntry.State == EventStoreEntryState.PublishFailed;
            return await GetEventsAsync(filter);
        }

        public async Task<IEnumerable<Event>> GetPublishedEventsAsync()
        {
            Expression<Func<EventStoreEntry, bool>> filter = (eventStoreEntry) => eventStoreEntry.State == EventStoreEntryState.Published;
            return await GetEventsAsync(filter);
        }
        
        public async Task MarkEventAsPublishedAsync(Event @event)
        {
            await UpdateEvent(@event, EventStoreEntryState.Published, true);
        }

        public async Task MarkEventAsFailedToPublishAsync(Event @event)
        {
            await UpdateEvent(@event, EventStoreEntryState.PublishFailed, true);
        }

        public async Task SaveEventAsync(Event @event, DbTransaction transaction)
        {
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction", $"A {typeof(DbTransaction).FullName} is required as a pre-requisite to save the event.");
            }

            var eventEntry = new EventStoreEntry(@event);

            this._context.Database.UseTransaction(transaction);
            this._context.EventEntries.Add(eventEntry);

            await this._context.SaveChangesAsync();
        }

        public async Task SaveEventAsync(Event @event)
        {
            //throw new NotSupportedException("Please use the SaveEventAsync with the DbTransaction parameter.");

            var eventEntry = new EventStoreEntry(@event);
            
            this._context.EventEntries.Add(eventEntry);

            await this._context.SaveChangesAsync();
        }

        private async Task UpdateEvent(Event @event, EventStoreEntryState newState, bool increaseSentTimesField)
        {
            var eventLogEntry = this._context.EventEntries.Single(ie => ie.EventId == @event.Id);
            if (increaseSentTimesField)
            {
                eventLogEntry.TimesSent++;
            }
            eventLogEntry.State = newState;

            this._context.EventEntries.Update(eventLogEntry);

            await this._context.SaveChangesAsync();
        }

        private async Task<IEnumerable<Event>> GetEventsAsync(Expression<Func<EventStoreEntry, bool>> filter)
        {
            if (filter == null)
            {
                filter = (eventStoreEntry) => true;
            }

            var eventEntries = await this._context.EventEntries
                .Where(filter)
                .ToListAsync();

            var events = new List<Event>(eventEntries.Count);

            foreach (var eventEntry in eventEntries)
            {
                var eventType = Assembly.GetEntryAssembly().GetType(eventEntry.EventTypeName);
                var @event = (Event)JsonConvert.DeserializeObject(eventEntry.Content, eventType);
                events.Add(@event);
            }

            return events;
        }

    }
}
