﻿using EventSourcing.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store.Service.Sql
{
    public class EventStoreContext : DbContext
    {
        public EventStoreContext(DbContextOptions<EventStoreContext> options) : base(options) { }

        public DbSet<EventStoreEntry> EventEntries { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("public");

            var eventStoreEntryBuilder = builder.Entity<EventStoreEntry>();

            eventStoreEntryBuilder.ToTable("event");

            eventStoreEntryBuilder.HasKey(m => m.EventId);
            eventStoreEntryBuilder.Property(m => m.EventId).HasColumnName("event_id").IsRequired();
            eventStoreEntryBuilder.Property(m => m.CreationTime).HasColumnName("creation_time").IsRequired();
            eventStoreEntryBuilder.Property(m => m.Content).HasColumnName("content").IsRequired();
            eventStoreEntryBuilder.Property(m => m.EventTypeName).HasColumnName("event_type").IsRequired();
            eventStoreEntryBuilder.Property(m => m.State).HasColumnName("state").IsRequired();
            eventStoreEntryBuilder.Property(m => m.TimesSent).HasColumnName("times_sent").IsRequired();

            base.OnModelCreating(builder);
        }


    }
}
