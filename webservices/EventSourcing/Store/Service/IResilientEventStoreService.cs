﻿using EventSourcing.Core;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store.Service
{
    public interface IResilientEventStoreService : IEventStoreService
    {
        Task SaveEventAsync(Event @event, DbTransaction connection);
    }
}
