﻿using EventSourcing.Core;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store.Service
{
    public interface IEventStoreService
    {
        Task SaveEventAsync(Event @event);

        Task<IEnumerable<Event>> GetNotPublishedEventsAsync();

        Task<IEnumerable<Event>> GetPublishedEventsAsync();

        Task MarkEventAsPublishedAsync(Event @event);

        Task MarkEventAsFailedToPublishAsync(Event @event);

    }
}
