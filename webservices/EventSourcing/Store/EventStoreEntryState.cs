﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store
{
    public enum EventStoreEntryState
    {
        PublishFailed = -1,
        NotPublished = 0,
        Published = 1
    }
}
