﻿using EventSourcing.Core;
using EventSourcing.Core.Common;
using EventSourcing.Store.Service;
using Microsoft.Extensions.Logging;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store.Worker
{
    public class EventStoreWorker : IEventStoreWorker
    {
        private readonly IEventStoreService _eventStoreService;
        private readonly IEventBus _eventBus;
        private readonly ILogger<EventStoreWorker> _logger;

        public EventStoreWorker(
            IEventStoreService service,
            IEventBus eventBus,
            ILogger<EventStoreWorker> logger)
        {
            this._eventStoreService = service;
            this._eventBus = eventBus;
            this._logger = logger;
        }

        public async Task ProcessNotPublishedEvents()
        {
            var unsentEvents = await this._eventStoreService.GetNotPublishedEventsAsync();

            this._logger.LogInformation($"Processing {unsentEvents.Count()} unsent events.");
            
            foreach (var unsentEvent in unsentEvents)
            {
                try
                {
                    this._logger.LogTrace($"Publishing event {unsentEvent.Id}.");
                    await this._eventBus.Publish(unsentEvent);

                    this._logger.LogTrace($"Marking event {unsentEvent.Id} as published in the persistence.");
                    await this._eventStoreService.MarkEventAsPublishedAsync(unsentEvent);
                }
                catch (InvalidOperationException ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await ProcessFailedToPublishEvent(unsentEvent);
                }
            }
        }

        private async Task ProcessFailedToPublishEvent(Event @event)
        {
            await this._eventStoreService.MarkEventAsFailedToPublishAsync(@event);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this._eventBus.Dispose();
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~EventStoreWorker() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
