﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Store.Worker
{
    public interface IEventStoreWorker : IDisposable
    {
        Task ProcessNotPublishedEvents();
    }
}
