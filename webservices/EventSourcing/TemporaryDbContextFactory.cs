﻿using EventSourcing.Store.Service.Sql;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing
{
    internal class TemporaryDbContextFactory : IDesignTimeDbContextFactory<EventStoreContext>
    {
        public EventStoreContext CreateDbContext(string[] options)
        {
            var builder = new DbContextOptionsBuilder<EventStoreContext>();
            builder.UseNpgsql("User ID=badbsoyjqyhluv;Password=5c0b1697bad64efffc467c0db8fa4b9bc2597da94e5bdc8f892f90f1d9119460;Host=ec2-79-125-117-53.eu-west-1.compute.amazonaws.com;Port=5432;Database=dlbe1jkgoonvu;SSL Mode=Require;Trust Server Certificate=true",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(EventStoreContext).GetTypeInfo().Assembly.GetName().Name));
            return new EventStoreContext(builder.Options);
        }
    }
}
