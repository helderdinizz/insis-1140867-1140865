﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EventSourcing.Migrations
{
    internal partial class InitialEventStore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "event",
                schema: "public",
                columns: table => new
                {
                    event_id = table.Column<Guid>(nullable: false),
                    content = table.Column<string>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    event_type = table.Column<string>(nullable: false),
                    state = table.Column<int>(nullable: false),
                    times_sent = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_event", x => x.event_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "event",
                schema: "public");
        }
    }
}
