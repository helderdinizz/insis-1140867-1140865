﻿using System;
using System.Threading.Tasks;

namespace EventSourcing.Core.Common
{
    public interface IEventBus : IDisposable
    {
        Task Publish(Event @event);

        Task Subscribe<T, TH>()
            where T : Event
            where TH : IEventHandler<T>;

        Task Unsubscribe<T, TH>()
            where T : Event
            where TH : IEventHandler<T>;

    }
}
