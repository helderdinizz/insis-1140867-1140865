﻿using System;

namespace EventSourcing.Core
{
    public abstract class Event
    {
        protected Event()
        {
            this.Id = Guid.NewGuid();
            this.OccurredAt = DateTime.UtcNow;
        }

        public Guid Id { get; set; }

        public DateTime OccurredAt { get; set; }
    }
}
