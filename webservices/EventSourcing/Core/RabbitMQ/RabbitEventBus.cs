﻿using EventSourcing.Core;
using EventSourcing.Core.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Core.RabbitMQ
{
    public class RabbitEventBus : IEventBus
    {
        private IRabbitConnection _connection;
        private ILogger<RabbitEventBus> _logger;
        private IConfiguration _configuration;
        private IModel _consumerChannel;

        private IEventBusSubscriptionManager _subscriptionManager;
        private readonly IServiceProvider _serviceProvider;
        private readonly string queueName;

        public RabbitEventBus(
            IRabbitConnection _connection,
            ILogger<RabbitEventBus> logger,
            IConfiguration configuration,
            IEventBusSubscriptionManager subscriptionsManager,
            IServiceProvider serviceProvider,
            string queueName = "")
        {
            this._connection = _connection;
            this._logger = logger;
            this._configuration = configuration;
            this.queueName = queueName;
            this._consumerChannel = CreateConsumerChannel();
            this._subscriptionManager = subscriptionsManager;
            this._serviceProvider = serviceProvider;
        }

        public Task Publish(Event @event)
        {
            if (!_connection.IsConnected)
            {
                _connection.TryConnect();
            }

            var policy = Policy.Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                    _logger.LogWarning(ex.ToString());
                })
                ;

            using (var channel = _connection.CreateModel())
            {
                var eventName = @event.GetType()
                    .Name;

                channel.ExchangeDeclare(exchange: "rabbitmq",
                                    type: ExchangeType.Direct,
                                    durable: true);

                var message = JsonConvert.SerializeObject(@event, Formatting.None);
                var body = Encoding.UTF8.GetBytes(message);

                var policyResult = policy.ExecuteAndCapture(() =>
                {
                    var properties = channel.CreateBasicProperties();
                    properties.DeliveryMode = 2; // persistent

                    channel.BasicPublish(exchange: "rabbitmq",
                                     routingKey: eventName,
                                     mandatory: true,
                                     basicProperties: properties,
                                     body: body);
                });

                if(policyResult.Outcome == OutcomeType.Failure)
                {
                    throw new InvalidOperationException("An exception occurred while publishing the event.", policyResult.FinalException);
                }
                
            }
            return Task.CompletedTask;
        }

        public Task Subscribe<T, TH>()
            where T : Event
            where TH : IEventHandler<T>
        {
            var eventName = _subscriptionManager.GetEventKey<T>();
            DoInternalSubscription(eventName);
            _subscriptionManager.AddSubscription<T, TH>();
            return Task.CompletedTask;
        }

        public Task Unsubscribe<T, TH>()
            where T : Event
            where TH : IEventHandler<T>
        {
            _subscriptionManager.RemoveSubscription<T, TH>();
            return Task.CompletedTask;
        }
        private void DoInternalSubscription(string eventName)
        {
            var containsKey = _subscriptionManager.HasSubscriptionsForEvent(eventName);
            if (!containsKey)
            {
                if (!_connection.IsConnected)
                {
                    _connection.TryConnect();
                }

                using (var channel = _connection.CreateModel())
                {
                    channel.QueueBind(queue: queueName,
                                      exchange: "rabbitmq",
                                      routingKey: eventName);
                }
            }
        }

        private IModel CreateConsumerChannel()
        {
            if (!_connection.IsConnected)
            {
                _connection.TryConnect();
            }

            var channel = _connection.CreateModel();

            channel.ExchangeDeclare(exchange: "rabbitmq",
                                 type: ExchangeType.Direct,
                                 durable: true);

            channel.QueueDeclare(queue: queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);


            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var eventName = ea.RoutingKey;
                var message = Encoding.UTF8.GetString(ea.Body);

                if(await ProcessEvent(eventName, message))
                {
                    channel.BasicAck(ea.DeliveryTag, multiple: false);
                }
                else
                {
                    channel.BasicNack(ea.DeliveryTag, multiple: false, requeue: true);
                }

                
            };

            channel.BasicConsume(queue: queueName,
                                 autoAck: false,
                                 consumer: consumer);

            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
            };

            return channel;
        }
        private async Task<bool> ProcessEvent(string eventName, string message)
        {
            if (_subscriptionManager.HasSubscriptionsForEvent(eventName))
            {
                var subscriptions = _subscriptionManager.GetHandlersForEvent(eventName);
                foreach (var subscription in subscriptions)
                {
                    var eventType = _subscriptionManager.GetEventTypeByName(eventName);
                    var integrationEvent = JsonConvert.DeserializeObject(message, eventType);
                    var handler = _serviceProvider.GetService(subscription.HandlerType);
                    var concreteType = typeof(IEventHandler<>).MakeGenericType(eventType);
                    await (Task)concreteType.GetMethod("Handle").Invoke(handler, new object[] { integrationEvent });
                    return true;
                }
            }
            return false;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_consumerChannel != null)
                    {
                        _consumerChannel.Dispose();
                    }
                }


                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
