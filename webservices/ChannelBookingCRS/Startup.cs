﻿using System;
using System.Net;

using ChannelBookingService.Configuration;
using ChannelBookingService.Gateway;
using ChannelBookingService.Helpers;
using EventSourcing.Core;
using EventSourcing.Core.Common;
using EventSourcing.Core.Common.Implementations;
using EventSourcing.Core.RabbitMQ;
using EventSourcing.Store.Service;
using EventSourcing.Store.Service.Sql;
using EventSourcing.Store.Worker;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace ChannelBooking
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .Enrich.FromLogContext()
                        .WriteTo.Seq("http://isep-insis-seq.ukwest.cloudapp.azure.com")
                        .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var jsonSerializerDefaultSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };

            JsonConvert.DefaultSettings = () => jsonSerializerDefaultSettings;

            services.AddMvcCore()
                .AddApiExplorer()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Formatting = jsonSerializerDefaultSettings.Formatting;
                    options.SerializerSettings.NullValueHandling = jsonSerializerDefaultSettings.NullValueHandling;
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm";
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                })
                .AddJsonFormatters();

            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new Info { Title = "Channel Booking Service", Version = "v1" });
                c.OrderActionsBy((apiDesc) => apiDesc.HttpMethod);
            });

            var persistenceConfig = this.Configuration.GetSection("PersistenceSettings").Get<PersistenceConfiguration>();

            services.AddSingleton<ICampaignGateway, CampaignGateway>();

            RegisterEventBus(services);

            services.AddDbContext<EventStoreContext>(options => options.UseNpgsql(persistenceConfig.ConnectionString, pgoptions =>
            {
                pgoptions.EnableRetryOnFailure(1);
            }), ServiceLifetime.Singleton, ServiceLifetime.Singleton);

            services.AddSingleton<IEventStoreService, SqlEventStoreService>();
            services.AddSingleton<IEventStoreWorker, EventStoreWorker>();

            //services.AddHangfire(x => x.UseSqlServerStorage(persistenceConfig.HangfireConnectionString, new SqlServerStorageOptions
            //{
            //    PrepareSchemaIfNecessary = true
            //}));
        }

        private void RegisterEventBus(IServiceCollection services)
        {
            services.AddSingleton<IEventBusSubscriptionManager, InMemoryEventBusSubscriptionManager>();

            services.AddSingleton<IRabbitConnection>(sp =>
            {
                var logger = sp.GetRequiredService<ILogger<RabbitConnection>>();

                var eventBusConfiguration = Configuration.GetSection("EventBus");

                var factory = new ConnectionFactory()
                {
                    HostName = eventBusConfiguration["Hostname"]
                };

                if (!string.IsNullOrEmpty(eventBusConfiguration["Username"]))
                {
                    factory.UserName = eventBusConfiguration["Username"];
                }

                if (!string.IsNullOrEmpty(eventBusConfiguration["Password"]))
                {
                    factory.Password = eventBusConfiguration["Password"];
                }

                return new RabbitConnection(factory, logger);
            });

            services.AddSingleton<IEventBus, RabbitEventBus>(sp =>
            {
                var rabbitConnection = sp.GetRequiredService<IRabbitConnection>();
                var logger = sp.GetRequiredService<ILogger<RabbitEventBus>>();
                var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionManager>();

                return new RabbitEventBus(rabbitConnection, logger, Configuration, eventBusSubcriptionsManager, sp, "bookings");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware(typeof(ErrorHandling));

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddSerilog();

            app.UseMvc();
            app.UseSwagger();

            GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(app.ApplicationServices));

            //app.UseHangfireServer();
            //app.UseHangfireDashboard();

            //this.StartHangfireJobs();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Channel Booking Service V1"));

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            
        }

        private void StartHangfireJobs()
        {
            RecurringJob.RemoveIfExists("process-events");
            RecurringJob.AddOrUpdate<IEventStoreWorker>("process-events", worker => worker.ProcessNotPublishedEvents(), Cron.MinuteInterval(5));
        }
    }
}
