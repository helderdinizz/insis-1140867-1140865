﻿namespace ChannelBookingService.Configuration
{
    public class PersistenceConfiguration
    {
        public string ConnectionString { get; set; }

        public string HangfireConnectionString { get; set; }

        public bool DropAndCreate { get; set; }
    }
}
