﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using ChannelBookingService.DTO;
using ChannelBookingService.Events;
using ChannelBookingService.Gateway;
using ChannelBookingService.Helpers;
using EventSourcing.Core;
using EventSourcing.Core.Common;
using EventSourcing.Store.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ChannelBooking.Controllers
{
    [Route("api/[controller]")]
    public class ChannelBookingController : ControllerBase
    {
        private readonly ICampaignGateway _campaignGateway;
        private readonly IEventStoreService _eventStoreService;
        private readonly ILogger<ChannelBookingController> _logger;

        public ChannelBookingController( 
            ICampaignGateway campaignGateway,
            IEventStoreService eventStoreService,
            ILogger<ChannelBookingController> logger)
        {
            this._campaignGateway = campaignGateway;
            this._eventStoreService = eventStoreService;
            this._logger = logger;
        }

        [HttpPost, Route("Book", Name = "BookChannel")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesHttpResponseType(HttpStatusCode.OK)]
        [ProducesHttpResponseType(HttpStatusCode.BadRequest)]
        [ProducesHttpResponseType(HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> BookChannel([FromBody]ChannelBookingRequest payload)
        {
            if (payload is null)
            {
                _logger.LogDebug("Payload is null.");
                throw new StatusCodeException("Payload sent is null.", HttpStatusCode.BadRequest);
            }

            var campaignExists = await this._campaignGateway.ExistsAsync(payload.CampaignID);
            if (!campaignExists)
            {
                _logger.LogDebug("Campaign {CampaignId} does not exist.", payload.CampaignID);
                throw new StatusCodeException($"Requested campaign ({payload.CampaignID}) does not exist.", HttpStatusCode.BadRequest);
            }

            _logger.LogInformation($"Publishing book request event for campaign {payload.CampaignID}.");

            await this._eventStoreService.SaveEventAsync(new ChannelBookRequestedEvent
            {
                DispatchChannels = payload.DispatchChannels,
                CampaignId = payload.CampaignID,
                DurationMinutes = payload.DurationMinutes,
                RequestId = Guid.NewGuid(),
                StartTime = payload.StartTime
            });

            _logger.LogInformation($"Successfully published book request event for campaign {payload.CampaignID}.");

            return Accepted();
        }
    }
}
