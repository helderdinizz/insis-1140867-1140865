﻿namespace ChannelBookingService.DTO
{
    public enum DispatchChannel
    {
        Email, SMS, SocialNetwork
    }
}
