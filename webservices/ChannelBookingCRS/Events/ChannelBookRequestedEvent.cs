﻿using ChannelBookingService.DTO;
using EventSourcing;
using EventSourcing.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelBookingService.Events
{
    public class ChannelBookRequestedEvent : Event
    {
        public Guid RequestId { get; set; }

        public Guid CampaignId { get; set; }

        public DateTime StartTime { get; set; }

        public int DurationMinutes { get; set; }

        public IEnumerable<DispatchChannel> DispatchChannels { get; set; }

    }
}
