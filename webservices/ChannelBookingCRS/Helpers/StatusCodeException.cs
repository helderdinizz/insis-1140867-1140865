﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace ChannelBookingService.Helpers
{
    [Serializable]
    public class StatusCodeException : Exception
    {
        public StatusCodeException(HttpStatusCode code = HttpStatusCode.InternalServerError) : this("An error has occurred. Please try again later.", code)
        {
        }

        public StatusCodeException(string message, HttpStatusCode code = HttpStatusCode.InternalServerError) : this(message, null, code)
        {

        }

        public StatusCodeException(string message, Exception innerException, HttpStatusCode code = HttpStatusCode.InternalServerError) : base(message, innerException)
        {
            this.StatusCode = code;
        }

        protected StatusCodeException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
        {

        }

        public HttpStatusCode StatusCode { get; } = HttpStatusCode.InternalServerError;
    }
}
