package pt.isep.eventsourcingframework.common;

import java.util.Date;
import java.util.UUID;

public abstract class Event {

    @Json
    private Date occurredAt;
    private UUID id;

    public Event(){
        this.id = UUID.randomUUID();
        this.occurredAt = new Date();

    }

    public
}
