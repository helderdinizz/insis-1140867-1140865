package pt.isep.eventsourcingframework.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitConnection {

    private final ConnectionFactory connectionFactory;
    private Connection connection;

    private static final Object lock = new Object();

    public RabbitConnection(ConnectionFactory connectionFactory){
        this.connectionFactory = connectionFactory;
    }

    public boolean isConnected(){
        return connection != null && connection.isOpen();
    }

    public Channel createChannel(){
        if(!isConnected()){
            throw new IllegalStateException("No RabbitMQ connections are available to perform this action");
        }

        try {
            return connection.createChannel();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean tryConnect(){
        System.out.println("Trying to connect...");

        int retryCount = 0;

        synchronized (lock){
            while(!isConnected() || retryCount < 5){
                try {
                    connection = connectionFactory.newConnection();

                    if(isConnected()){
                        addListeners();
                    }
                    return true;
                } catch (IOException | TimeoutException e) {
                    System.out.println(e.getMessage());
                    try {
                        Thread.sleep(retryCount * 1000 * (int)Math.pow(2, retryCount));
                    } catch (InterruptedException e1) {
                        System.out.println(e.getMessage());
                    }
                } finally{
                    retryCount++;
                }
            }
            return false;
        }
    }

    private void addListeners(){
        connection.addBlockedListener(new BlockedListener() {
            @Override
            public void handleBlocked(String s) throws IOException {
                System.out.println("Connection shutdown. Reconnecting...");
                tryConnect();
            }

            @Override
            public void handleUnblocked() throws IOException {
                System.out.println("Connection restored.");
            }
        });

        connection.addShutdownListener(e -> {
            System.out.println("Connection shutdown. Reconnecting...");
            tryConnect();
        });

        connection.addBlockedListener(s -> {
            System.out.println("Connection threw exception. Reconnecting...");
            tryConnect();
        }, () -> System.out.println("Connection restored."));
    }

}

