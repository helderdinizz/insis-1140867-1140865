package pt.isep.eventsourcingframework.common;

public class SubscriptionInfo {

    private Class handlerType;

    private SubscriptionInfo(Class handlerType)
    {
        this.handlerType = handlerType;
    }


    public static SubscriptionInfo Typed(Class handlerType)
    {
        return new SubscriptionInfo( handlerType);
    }

    public Class getHandlerType(){
        return this.handlerType;
    }
}
