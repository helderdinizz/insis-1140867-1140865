package pt.isep.eventsourcingframework.common.impl;

import pt.isep.eventsourcingframework.common.Event;
import pt.isep.eventsourcingframework.common.EventBusSubscriptionManager;
import pt.isep.eventsourcingframework.common.EventHandler;
import pt.isep.eventsourcingframework.common.SubscriptionInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public class InMemoryEventBusSubscriptionManager implements EventBusSubscriptionManager {
    private final Map<String, List<SubscriptionInfo>> handlers;
    private final List<Class<? extends Event>> eventTypes;

    public InMemoryEventBusSubscriptionManager(){
        this.handlers = new HashMap<String, List<SubscriptionInfo>>();
        this.eventTypes = new ArrayList<Class<? extends Event>>();
    }

    public boolean isEmpty() {
        return this.handlers.isEmpty();
    }

    public <T extends Event, TH extends EventHandler<T>> void addSubscription(Class<T> event, TH eventHandler) {
        String eventName = getEventKey(event);
        if (!hasSubscriptionsForEvent(eventName))
        {
            this.handlers.put(eventName, new ArrayList<SubscriptionInfo>());
        }

        if (this.handlers.get(eventName).stream().anyMatch(s -> s.getHandlerType().equals(eventHandler.getClass())))
        {
            throw new IllegalArgumentException(
                    "Handler Type" +  eventHandler.getClass().getSimpleName() + "already registered for '" + eventName + "'");
        }

        this.handlers.get(eventName).add(SubscriptionInfo.Typed(eventHandler.getClass()));
        this.eventTypes.add(event);

    }

    public <T extends Event, TH extends EventHandler<T>> void removeSubscription(Class<T> event, TH eventHandler) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public <T extends Event> boolean hasSubscriptionsForEvent(Class<T> event) {
        return hasSubscriptionsForEvent(getEventKey(event));
    }

    public boolean hasSubscriptionsForEvent(String eventName) {
        return handlers.containsKey(eventName) && !handlers.get(eventName).isEmpty();
    }

    public Class getEventClassByName(String eventName) {
        return eventTypes.stream().filter(e -> e.getSimpleName().equals(eventName)).findFirst().orElse(null);
    }

    public void clear() {
        this.handlers.clear();
    }

    public List<SubscriptionInfo> getHandlersForEvent() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public List<SubscriptionInfo> getHandlersForEvent(String eventName) {
        return this.handlers.get(eventName);
    }

    public <T extends Event> String getEventKey(Class<T> event) {
        return event.getSimpleName();
    }
}
