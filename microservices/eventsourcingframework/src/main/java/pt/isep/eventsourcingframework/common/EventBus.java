package pt.isep.eventsourcingframework.common;

import java.util.concurrent.Future;

public interface EventBus {
    Future publish(Event event);
    <T extends Event, TH extends EventHandler<T>> Future subscribe(Class<T> event, TH eventHandler);
    <T extends Event, TH extends EventHandler<T>> Future unsubscribe(Class<T> event, TH eventHandler);
}
