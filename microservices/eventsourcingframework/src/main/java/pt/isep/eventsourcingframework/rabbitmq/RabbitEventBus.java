package pt.isep.eventsourcingframework.rabbitmq;

import pt.isep.eventsourcingframework.common.Event;
import pt.isep.eventsourcingframework.common.EventBus;
import pt.isep.eventsourcingframework.common.EventBusSubscriptionManager;
import pt.isep.eventsourcingframework.common.EventHandler;

import java.util.concurrent.Future;

public class RabbitEventBus implements EventBus {

    public RabbitEventBus(RabbitConnection rabbitConnection, EventBusSubscriptionManager manager, String queueName){

    }

    @Override
    public Future publish(Event event) {
        return null;
    }

    @Override
    public <T extends Event, TH extends EventHandler<T>> Future subscribe(Class<T> event, TH eventHandler) {
        return null;
    }

    @Override
    public <T extends Event, TH extends EventHandler<T>> Future unsubscribe(Class<T> event, TH eventHandler) {
        return null;
    }
}
