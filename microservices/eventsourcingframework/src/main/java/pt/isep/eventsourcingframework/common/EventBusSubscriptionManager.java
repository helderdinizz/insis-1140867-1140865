package pt.isep.eventsourcingframework.common;

import java.util.List;
import java.util.concurrent.Future;

public interface EventBusSubscriptionManager {
    boolean isEmpty();

    <T extends Event, TH extends EventHandler<T>> void addSubscription(Class<T> event, TH eventHandler);

    <T extends Event, TH extends EventHandler<T>> void removeSubscription(Class<T> event, TH eventHandler);

    <T extends Event> boolean hasSubscriptionsForEvent(Class<T> event);

    boolean hasSubscriptionsForEvent(String eventName);
    Class getEventClassByName(String eventName);

    void clear();

    List<SubscriptionInfo> getHandlersForEvent();
    List<SubscriptionInfo> getHandlersForEvent(String eventName);

    <T extends Event> String getEventKey(Class<T> event);
}
