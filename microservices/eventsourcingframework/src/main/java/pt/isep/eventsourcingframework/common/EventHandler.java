package pt.isep.eventsourcingframework.common;

import java.util.concurrent.Future;

public interface EventHandler<T extends Event> {
    Future handle(T event);
}
