package pt.isep.insis.links.domain;

import org.junit.Test;
import pt.isep.insis.links.service.AppSettings;

import static org.junit.Assert.*;

public class RedirectUrlBuilderTest {

    @Test
    public void buildRedirectUrl() {
        String campaignId = "testId";
        String userId = "userTestId";

        String testUrl = AppSettings.instance().getLinkBaseUrl() + "?campaign=" + campaignId + "&user=" + userId;
        String expectedUrl = RedirectUrlBuilder.buildRedirectUrl(campaignId, userId);
        assertEquals(testUrl, expectedUrl);
    }
}