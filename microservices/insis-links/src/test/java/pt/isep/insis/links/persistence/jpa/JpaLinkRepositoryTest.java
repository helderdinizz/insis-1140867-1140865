package pt.isep.insis.links.persistence.jpa;

import org.junit.Before;
import org.junit.Test;
import pt.isep.insis.links.domain.Link;
import pt.isep.insis.links.domain.RedirectUrlBuilder;
import pt.isep.insis.links.persistence.PersistenceContext;

import static org.junit.Assert.*;

public class JpaLinkRepositoryTest {

    private JpaLinkRepository repository;
    private Link link1;
    private Link link2;
    private String campaignId = "UNIT-TEST-Campaign";
    private String userId = "UNIT-TEST-User";

    @Before
    public void setUp() throws Exception {
        repository = new JpaLinkRepository();
        link1 = new Link(campaignId, userId);
        link2 = new Link();
        PersistenceContext.repositories().links().add(link1);
        PersistenceContext.repositories().links().add(link2);
    }

    @Test
    public void getLinkSuccessfully() {
        String redirectUrl = RedirectUrlBuilder.buildRedirectUrl(link1.getCampaignId(), link1.getUserId());
        Link link = repository.getLink(redirectUrl);

        assertNotNull("The link shouldn't be null", link);
        assertEquals("The redirectUrl should be the same", link.getRedirectUrl(), redirectUrl);
    }

    @Test
    public void getLinkV2() {
        Link link = repository.getLink(campaignId, userId);
        assertNotNull("The link shouldn't be null", link);
    }
}