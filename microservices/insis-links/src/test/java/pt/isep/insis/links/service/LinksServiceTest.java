package pt.isep.insis.links.service;

import org.junit.Test;
import pt.isep.insis.links.domain.dto.LinkDTO;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

public class LinksServiceTest {

    private LinksService service = new LinksService();
    String campaignId = "d3t41ls";
    String userId = "testUser";

    @Test
    public void generateSuccessful() {
        Response response = this.service.generate(campaignId, userId);

        assertNotEquals("Testing if the response isn't 400", Response.Status.BAD_REQUEST, response.getStatus());
        assertEquals("Testing if the response is 200", Response.Status.OK.getStatusCode(), response.getStatus());
        LinkDTO responseDto = (LinkDTO) response.getEntity();
        boolean hasAnyResult = !(responseDto.url.isEmpty());

        System.out.println("Response String: " + responseDto.toString());
        assertTrue("Has any response", hasAnyResult);
    }

    @Test
    public void generateTheSame() {
        Response response = this.service.generate(campaignId, userId);
        LinkDTO responseDto = (LinkDTO) response.getEntity();
        boolean hasAnyResult = !(responseDto.url.isEmpty());
        assertTrue("Has any response", hasAnyResult);
    }
}