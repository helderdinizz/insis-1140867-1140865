package pt.isep.insis.links.persistence;

public interface Repository<T, PK> {
    T save(T entity);
    boolean add(T entity);
    Iterable<T> all();
    T findById(PK id);
    long size();
}
