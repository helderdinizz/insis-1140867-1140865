package pt.isep.insis.links.domain.dto;

public interface DTO<T> {
    T toDTO();
}
