package pt.isep.insis.links.persistence.jpa;

import pt.isep.insis.links.persistence.LinkRepository;
import pt.isep.insis.links.persistence.RepositoryFactory;

public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public LinkRepository links() {
        return new JpaLinkRepository();
    }
}
