package pt.isep.insis.links.persistence;

import pt.isep.insis.links.service.AppSettings;

public final class PersistenceContext {
    private PersistenceContext() {

    }

    public static RepositoryFactory repositories() {
        final String factoryClassName = AppSettings.instance().getRepositoryFactory();
        try {
            return (RepositoryFactory) Class.forName(factoryClassName).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            // TODO handle exception
            return null;
        }
    }
}
