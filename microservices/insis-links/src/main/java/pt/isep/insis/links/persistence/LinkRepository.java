package pt.isep.insis.links.persistence;

import pt.isep.insis.links.domain.Link;

public interface LinkRepository extends Repository<Link, Integer> {
    Link getLink(String link);

    Link getLink(String campaignId, String userId);
}
