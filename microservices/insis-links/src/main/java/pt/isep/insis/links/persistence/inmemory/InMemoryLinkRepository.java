package pt.isep.insis.links.persistence.inmemory;

import pt.isep.insis.links.domain.Link;
import pt.isep.insis.links.persistence.LinkRepository;

public class InMemoryLinkRepository extends InMemoryRepository<Link, Integer> implements LinkRepository {

    protected static Integer entityCounter = 0;

    public InMemoryLinkRepository() {
    }

    @Override
    protected Integer newPK(Link link) {
        return incrementEntityCounter();
    }

    @Override
    public Link findById(Integer id) {
        Iterable<Link> allLinks = super.all();
        for (Link link : allLinks) {
            if (link.getId().equals(id)) {
                return link;
            }
        }
        return null;
    }

    @Override
    public Link getLink(String link) {
        Iterable<Link> allLinks = super.all();
        for (Link linkObj : allLinks) {
            if (linkObj.getRedirectUrl().equals(link)) {
                return linkObj;
            }
        }
        return null;
    }

    @Override
    public Link getLink(String campaignId, String userId) {
        Iterable<Link> allLinks = super.all();
        for (Link linkObj : allLinks) {
            if (linkObj.getCampaignId().equals(campaignId) && linkObj.getUserId().equals(userId)) {
                return linkObj;
            }
        }
        return null;
    }

    private static synchronized Integer incrementEntityCounter() {
        return ++entityCounter;
    }
}
