package pt.isep.insis.links.persistence.inmemory;

import pt.isep.insis.links.persistence.DeletableRepository;
import pt.isep.insis.links.persistence.Repository;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class InMemoryRepository<T, K> implements Repository<T, K>, DeletableRepository<T, K> {

    private final Map<K, T> repository = new HashMap<K, T>();

    @Override
    public void delete(T entity) {
        this.repository.remove(entity);
    }

    @Override
    public void deleteById(K entityId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T save(T entity) {
        this.repository.put(newPK(entity), entity);
        return entity;
    }

    @Override
    public Iterable<T> all() {
        return this.repository.values();
    }

    @Override
    public T findById(K id) {
        return repository.get(id);
    }

    @Override
    public long size() {
        return this.repository.size();
    }

    @Override
    public boolean add(T entity) {
        K pk = newPK(entity);
        this.repository.put(pk, entity);
        Field entityField = null;
        //TODO refactor this with time, or just remove it idk
        try {
            entityField = entity.getClass().getDeclaredField("id");
            entityField.setAccessible(true);
            entityField.set(entity, pk);
            entityField.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException exception) {
            Logger.getLogger(InMemoryRepository.class.getName()).log(Level.INFO, "No declared field found in this class. Checking super class");
            try {
                entityField = entity.getClass().getSuperclass().getDeclaredField("id");
                entityField.setAccessible(true);
                entityField.set(entity, pk);
                entityField.setAccessible(false);
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                Logger.getLogger(InMemoryRepository.class.getName()).log(Level.INFO, "No existing ID fields" + ":", ex);
            }
        }

        return true;
    }

    protected abstract K newPK(T entity);

}

