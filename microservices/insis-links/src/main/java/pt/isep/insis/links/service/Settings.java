package pt.isep.insis.links.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Settings {
    protected Properties appProperties = new Properties();

    protected Settings() {
    }

    public String getProperty(String key) {
        return this.appProperties.getProperty(key);
    }

    public Properties getPropertiesMatching(String regex) {
        Properties properties = new Properties();
        Iterator itr = this.appProperties.keySet().iterator();
        while (itr.hasNext()) {
            String key = (String) itr.next();
            if (key.matches(regex)) {
                String property = this.appProperties.getProperty(key);
                properties.setProperty(key, property);
            }
        }
        return properties;
    }

    public void setProperty(String key, String value) {
        this.appProperties.setProperty(key, value);
    }

    protected void loadProperties(final String PROPERTIES_RESOURCE, ClassLoader classLoader) {
        InputStream propertiesStream = null;
        try {
            propertiesStream = classLoader.getResourceAsStream(PROPERTIES_RESOURCE);
            if (propertiesStream != null) {
                appProperties.load(propertiesStream);
            } else {
                Logger.getLogger(Settings.class.getClass().getName()).log(Level.WARNING, "File not found. Proceeding with the default properties.");
                setDefaultProperties();
            }
        } catch (final IOException exio) {
            setDefaultProperties();
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, exio);
        } finally {
            if (propertiesStream != null) {
                try {
                    propertiesStream.close();
                } catch (final IOException ex) {
                    Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    protected abstract void setDefaultProperties();

}
