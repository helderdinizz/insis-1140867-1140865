package pt.isep.insis.links.persistence.jpa;

import pt.isep.insis.links.domain.Link;
import pt.isep.insis.links.persistence.LinkRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

public class JpaLinkRepository extends JpaRepository<Link, Integer> implements LinkRepository {

    @Override
    protected String persistenceUnitName() {
        return JpaPersistenceSettings.PERSISTENCE_UNIT_NAME;
    }


    @Override
    public Link getLink(String campaignId, String userId) {
        if (campaignId.isEmpty() || userId.isEmpty()) {
            return null;
        }

        try {
            EntityManagerFactory entityManagerFactory = super.entityManagerFactory();
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            CriteriaBuilder criteria = entityManager.getCriteriaBuilder();

            CriteriaQuery<Link> criteriaQuery = criteria.createQuery(Link.class);
            Root<Link> linkRoot = criteriaQuery.from(Link.class);
            ParameterExpression<String> parameterCampaign = criteria.parameter(String.class);
            ParameterExpression<String> parameterUser = criteria.parameter(String.class);

            criteriaQuery.select(linkRoot).where(
                    criteria.equal(linkRoot.get("campaignId"), parameterCampaign),
                    criteria.equal(linkRoot.get("userId"), parameterUser)
            );

            TypedQuery<Link> query = entityManager.createQuery(criteriaQuery);
            query.setParameter(parameterCampaign, campaignId);
            query.setParameter(parameterUser, userId);

            return query.getSingleResult();
        } catch (Exception ex) { // Whatever exception it throws doesn't matter, just return null.
            return null;
        }
    }

    @Override
    public Link getLink(String link) {
        if (link.isEmpty()) {
            return null;
        }

        try {
            EntityManagerFactory entityManagerFactory = super.entityManagerFactory();
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            CriteriaBuilder criteria = entityManager.getCriteriaBuilder();

            CriteriaQuery<Link> criteriaQuery = criteria.createQuery(Link.class);
            Root<Link> linkRoot = criteriaQuery.from(Link.class);
            ParameterExpression<String> parameter = criteria.parameter(String.class);

            criteriaQuery.select(linkRoot).where(criteria.equal(linkRoot.get("redirectUrl"), parameter));

            TypedQuery<Link> query = entityManager.createQuery(criteriaQuery);
            query.setParameter(parameter, link);

            return query.getSingleResult();
        } catch (Exception ex) { // Whatever exception it throws doesn't matter, just return null.
            return null;
        }
    }
}
