package pt.isep.insis.links.persistence;

public interface RepositoryFactory {
    LinkRepository links();
}
