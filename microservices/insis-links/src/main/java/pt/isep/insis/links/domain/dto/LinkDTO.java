package pt.isep.insis.links.domain.dto;

public class LinkDTO {
    public String url;
    public String campaignId;
    public String userId;

    public LinkDTO(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "{ url: " + url + " | campaignId: " + campaignId + " | userId: " + userId + " }";
    }

}
