package pt.isep.insis.links.persistence.inmemory;

import pt.isep.insis.links.persistence.LinkRepository;
import pt.isep.insis.links.persistence.RepositoryFactory;

public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static LinkRepository linkRepository = null;

    @Override
    public synchronized LinkRepository links() {
        if (linkRepository == null) {
            linkRepository = new InMemoryLinkRepository();
        }
        return linkRepository;
    }
}
