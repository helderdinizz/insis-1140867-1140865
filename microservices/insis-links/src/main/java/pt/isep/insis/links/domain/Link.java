package pt.isep.insis.links.domain;

import pt.isep.insis.links.domain.dto.DTO;
import pt.isep.insis.links.domain.dto.LinkDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class Link implements DTO, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String campaignId;
    private String userId;

    @Column(unique = true)
    private String redirectUrl;

    @Temporal(TemporalType.DATE)
    private Date createdAt;

    public Link() {
        this.createdAt = new Date();
    }

    /**
     * Builds a new 'Link' by building the URL automatically with the given parameters.
     *
     * @param campaignId
     * @param userId
     */
    public Link(String campaignId, String userId) {
        this.campaignId = campaignId;
        this.userId = userId;
        this.createdAt = new Date();
        this.redirectUrl = this.buildUrl();
    }

    /**
     * Builds a new 'Link'.
     *
     * @param campaignId
     * @param userId
     * @param redirectUrl
     */
    public Link(String campaignId, String userId, String redirectUrl) {
        this.campaignId = campaignId;
        this.userId = userId;
        this.redirectUrl = redirectUrl;
        this.createdAt = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "{ " +
                "URL: " + this.redirectUrl +
                " | " +
                "CampaignId: " + this.campaignId +
                " | " +
                "UserId: " + this.userId +
                " | " +
                " }";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return Objects.equals(campaignId, link.campaignId) &&
                Objects.equals(userId, link.userId) &&
                Objects.equals(redirectUrl, link.redirectUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaignId, userId, redirectUrl);
    }

    private String buildUrl() {
        return RedirectUrlBuilder.buildRedirectUrl(this.campaignId, this.userId);
    }

    @Override
    public LinkDTO toDTO() {
        return new LinkDTO(this.getRedirectUrl());
    }
}
