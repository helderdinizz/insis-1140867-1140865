/*
 * Copyright (c) 2016, WSO2 Inc. (http://wso2.com) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pt.isep.insis.links.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import org.springframework.stereotype.Component;
import pt.isep.insis.links.domain.Link;
import pt.isep.insis.links.domain.dto.LinkDTO;
import pt.isep.insis.links.persistence.PersistenceContext;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Links Service class.
 *
 * @since 0.1-SNAPSHOT
 */
@Component
@Api(value = "/links")
@SwaggerDefinition(
        info = @Info(
                title = "INSIS Links Definition",
                version = "1.0.0",
                description = "INSIS Links Microservice"
        )
)
@Path("/links")
public class LinksService {

    public LinksService() {
    }

    /**
     * Returns the list of all existing links
     *
     * @return List of all existing links
     */
    @GET
    @Path("/")
    @Produces("application/json")
    @ApiOperation(
            value = "Get all links",
            notes = "Shows the list of existing links",
            response = Link.class,
            responseContainer = "List"
    )
    public Response allLinks() {
        System.out.println(LinksService.class.getSimpleName() + " ALL LINKS invoked");

        Iterator<Link> iterator = PersistenceContext.repositories().links().all().iterator();
        List<Link> allLinks = new ArrayList<>();
        iterator.forEachRemaining(allLinks::add);

        return Response.ok().entity(allLinks).build();

    }

    /**
     * Generates a redirect url for the campaign and user combination
     *
     * @param campaignId
     * @param userId
     * @return
     */
    @GET
    @Path("/generate")
    @Produces("application/json")
    @ApiOperation(
            value = "Generate link",
            notes = "Generates a link for the given campaignId and userId",
            response = LinkDTO.class
    )
    public Response generate(@QueryParam("campaignId") String campaignId,
                             @QueryParam("userId") String userId) {
        System.out.println(LinksService.class.getSimpleName() + " GENERATE invoked");

        Link link = PersistenceContext.repositories().links().getLink(campaignId, userId);

        if (link == null) {
            link = new Link(campaignId, userId);
            PersistenceContext.repositories().links().add(link);
        }

        return Response.ok().entity(link.toDTO()).build();
    }
}
