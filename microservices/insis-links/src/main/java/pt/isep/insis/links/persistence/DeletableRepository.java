package pt.isep.insis.links.persistence;

public interface DeletableRepository<T, PK> extends Repository<T, PK> {
    void delete(T entity);

    void deleteById(PK entityId);
}
