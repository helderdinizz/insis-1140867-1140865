package pt.isep.insis.analytics.utils;

public class UuidValidator {
    public static boolean validate(String uuid) {
        return uuid.matches("/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/\n");
    }
}
