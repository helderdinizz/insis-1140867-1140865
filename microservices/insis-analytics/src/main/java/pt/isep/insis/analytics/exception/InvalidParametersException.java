package pt.isep.insis.analytics.exception;

public class InvalidParametersException extends Exception {
    public InvalidParametersException() {
        super("| > Parameters are invalid!");
    }

    public InvalidParametersException(String message) {
        super(message);
    }

    public InvalidParametersException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidParametersException(Throwable cause) {
        super(cause);
    }

    protected InvalidParametersException(String message, Throwable cause,
                                         boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
