package pt.isep.insis.analytics.service;

public class AppSettings extends Settings {

    private static final String PROPERTIES_RESOURCE = "analytics.properties";
    private static final String REPOSITORY_FACTORY_KEY = "persistence.repositoryFactory";
    private static final String LINK_BASE_URL_KEY = "link.base.url";
    private static final String MICROSERVICE_PORT = "microservice.port";
    private static final String PERSISTENCE_UNIT = "persistence.unit";

    private AppSettings() {
        loadProperties(PROPERTIES_RESOURCE, AppSettings.class.getClassLoader());
    }

    //Initialization-on-demand Pattern -- Lazy-loaded Singleton
    public static AppSettings instance() {
        return LazyHolder.INSTANCE;
    }

    private static class LazyHolder {
        protected static final AppSettings INSTANCE = new AppSettings();
    }

    @Override
    protected void setDefaultProperties() {
        //TODO: DEFINE THE DEFAULT PROPERTIES
        //appProperties.setProperty(REPOSITORY_FACTORY_KEY, "");
    }

    public String getRepositoryFactory() {
        return appProperties.getProperty(REPOSITORY_FACTORY_KEY);
    }

    public String getLinkBaseUrl() {
        return appProperties.getProperty(LINK_BASE_URL_KEY);
    }

    public String getMicroservicePort() {
        return appProperties.getProperty(MICROSERVICE_PORT);
    }

    public String getPersistenceUnit() {
        return appProperties.getProperty(PERSISTENCE_UNIT);
    }
}
