/*
 * Copyright (c) 2016, WSO2 Inc. (http://wso2.com) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pt.isep.insis.analytics.service;

import io.swagger.annotations.Api;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import org.springframework.stereotype.Component;
import pt.isep.insis.analytics.domain.Analytics;
import pt.isep.insis.analytics.domain.RedirectUrlBuilder;
import pt.isep.insis.analytics.exception.InvalidParametersException;
import pt.isep.insis.analytics.persistence.PersistenceContext;
import pt.isep.insis.analytics.utils.UuidValidator;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
@Api(value = "/analytics")
@SwaggerDefinition(
        info = @Info(
                title = "INSIS Analytics Definition",
                version = "1.0.0",
                description = "INSIS Analytics Microservice"
        )
)
@Path("/analytics")
public class AnalyticsService {

    public AnalyticsService() {
    }

    @GET
    @Path("/")
    @Produces("application/json")
    public Response allAnalytics() {
        System.out.println("| ALL ANALYTICS INVOKED");

        Iterator<Analytics> iterator = PersistenceContext.repositories().analytics().all().iterator();
        List<Analytics> allAnalytics = new ArrayList<>();
        iterator.forEachRemaining(allAnalytics::add);

        return Response.ok().entity(allAnalytics).build();
    }

    @GET
    @Path("/visitCampaign")
    public Response visitCampaign(@QueryParam("campaignId") String campaignId,
                                  @QueryParam("userId") String userId)
            throws InvalidParametersException {
        System.out.println("| VISIT CAMPAIGN invoked");

        if (campaignId == null || userId == null || campaignId.isEmpty() || userId.isEmpty()) {
            String errorMessage = "Parameters are invalid! {campaignId:" + campaignId + " / userId: " + userId + " }";
            throw new InvalidParametersException(errorMessage);
        }

        Analytics possibleAnalytics = PersistenceContext.repositories().analytics().findAnalytics(campaignId, userId);
        if (possibleAnalytics != null) {
            System.out.println("| > Analytics received are already on the database.");
            if (!possibleAnalytics.isVisited()) {
                PersistenceContext.repositories().analytics().markAsVisited(campaignId, userId);
            }
            System.out.println("| > Analytics received are already on the database and have been visited by the user.");
            return buildRedirectUri(possibleAnalytics).build();
        }

        System.out.println("| > Analytics received are not on the database, creating...");
        Analytics newAnalytics = new Analytics(campaignId, userId);
        PersistenceContext.repositories().analytics().add(newAnalytics);

        return buildRedirectUri(newAnalytics).build();
    }

    private Response.ResponseBuilder buildRedirectUri(Analytics analyticsObj) {
        String redirectUrl = RedirectUrlBuilder.buildRedirectUrl(analyticsObj.getCampaignId(), analyticsObj.getUserId());
        System.out.println("| > Generated the following URL: " + redirectUrl);
        URI uri = URI.create(redirectUrl);
        return Response.temporaryRedirect(uri);
    }

    private boolean validateVisitCampaignParameters(String campaignId, String userId) {
        return campaignId != null && userId != null &&
                !campaignId.isEmpty() && !userId.isEmpty();
        // TODO Add it after testing
        //UuidValidator.validate(campaignId) && UuidValidator.validate(userId);
    }


}
