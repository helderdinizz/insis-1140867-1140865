package pt.isep.insis.analytics.domain.DTO;

public interface DTO<T> {
    T toDTO();
}
