package pt.isep.insis.analytics.domain;

import pt.isep.insis.analytics.domain.DTO.AnalyticsDTO;
import pt.isep.insis.analytics.domain.DTO.DTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class Analytics implements DTO, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String campaignId;
    private String userId;
    private boolean visited;
    private String redirectUrl; //maybe there's no need to have it
    @Temporal(TemporalType.DATE)
    private Date visitedAt;

    public Analytics() {
    }

    public Analytics(String campaignId, String userId) {
        this.campaignId = campaignId;
        this.userId = userId;
        this.markAsVisited();
    }

    public Integer getId() {
        return id;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public boolean isVisited() {
        return visited;
    }

    public Date getVisitedAt() {
        return visitedAt;
    }

    public boolean markAsVisited() {
        if (isVisited()) {
            return false;
        }
        this.visited = true;
        this.visitedAt = new Date();
        return this.visited;
    }

    @Override
    public String toString() {
        return "{ " +
                "CampaignId: " + this.campaignId +
                " | " +
                "UserId: " + this.userId +
                " | " +
                "Visited: " + this.visited +
                " }";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Analytics link = (Analytics) o;
        return Objects.equals(campaignId, link.campaignId) &&
                Objects.equals(userId, link.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaignId, userId);
    }

    @Override
    public AnalyticsDTO toDTO() {
        AnalyticsDTO dto = new AnalyticsDTO();
        dto.id = getId();
        dto.campaignId = getCampaignId();
        dto.userId = getUserId();
        dto.visited = isVisited();
        dto.visitedAt = getVisitedAt();
        return dto;
    }
}
