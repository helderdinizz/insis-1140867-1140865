package pt.isep.insis.analytics.exception.mapper;

import pt.isep.insis.analytics.exception.InvalidParametersException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidParametersMapper implements ExceptionMapper<InvalidParametersException> {
    public Response toResponse(InvalidParametersException ex) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(ex.getMessage() + " [from InvalidParametersMapper]")
                .type("text/plain")
                .build();
    }
}
