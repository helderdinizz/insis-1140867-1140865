package pt.isep.insis.analytics.persistence.jpa;

import pt.isep.insis.analytics.service.AppSettings;

public class JpaPersistenceSettings {
    private static String PERSISTENCE_UNIT = "persistence.unit";
    protected static final String PERSISTENCE_UNIT_NAME = getPersistenceUnit();

    private JpaPersistenceSettings() {
    }

    private static String getPersistenceUnit() {
        return AppSettings.instance().getProperty(PERSISTENCE_UNIT);
    }
}
