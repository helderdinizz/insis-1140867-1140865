package pt.isep.insis.analytics.persistence;

public interface DeletableRepository<T, PK> extends Repository<T, PK> {
    void delete(T entity);

    void deleteById(PK entityId);
}
