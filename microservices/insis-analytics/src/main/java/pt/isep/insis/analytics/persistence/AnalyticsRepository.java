package pt.isep.insis.analytics.persistence;

import pt.isep.insis.analytics.domain.Analytics;

public interface AnalyticsRepository extends Repository<Analytics, Integer> {

    Analytics findAnalytics(String campaignId, String userId);

    boolean markAsVisited(String campaignId, String userId);

    boolean markAsVisitedById(Integer id);
}
