package pt.isep.insis.analytics.persistence;

public interface RepositoryFactory {
    AnalyticsRepository analytics();
}
