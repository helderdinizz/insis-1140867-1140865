package pt.isep.insis.analytics.persistence.jpa;

import pt.isep.insis.analytics.persistence.AnalyticsRepository;
import pt.isep.insis.analytics.persistence.RepositoryFactory;

public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public AnalyticsRepository analytics() {
        return new JpaAnalyticsRepository();
    }
}
