package pt.isep.insis.analytics.persistence.jpa;

import pt.isep.insis.analytics.domain.Analytics;
import pt.isep.insis.analytics.persistence.AnalyticsRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

public class JpaAnalyticsRepository extends JpaRepository<Analytics, Integer> implements AnalyticsRepository {

    @Override
    protected String persistenceUnitName() {
        return JpaPersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Analytics findAnalytics(String campaignId, String userId) {
        if (campaignId.isEmpty() || userId.isEmpty()) {
            return null;
        }

        try {
            EntityManagerFactory entityManagerFactory = super.entityManagerFactory();
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            CriteriaBuilder criteria = entityManager.getCriteriaBuilder();

            CriteriaQuery<Analytics> criteriaQuery = criteria.createQuery(Analytics.class);
            Root<Analytics> analyticsRoot = criteriaQuery.from(Analytics.class);
            ParameterExpression<String> parameterCampaign = criteria.parameter(String.class);
            ParameterExpression<String> parameterUser = criteria.parameter(String.class);

            criteriaQuery.select(analyticsRoot).where(
                    criteria.equal(analyticsRoot.get("campaignId"), parameterCampaign),
                    criteria.equal(analyticsRoot.get("userId"), parameterUser)
            );

            TypedQuery<Analytics> query = entityManager.createQuery(criteriaQuery);
            query.setParameter(parameterCampaign, campaignId);
            query.setParameter(parameterUser, userId);

            return query.getSingleResult();
        } catch (Exception ex) { // Whatever exception it throws doesn't matter, just return null.
            return null;
        }
    }

    @Override
    public boolean markAsVisited(String campaignId, String userId) {
        Analytics analytics = findAnalytics(campaignId, userId);
        if (analytics != null) {
            return this.visitedMarkerHelper(analytics);
        } else {
            return false;
        }
    }

    @Override
    public boolean markAsVisitedById(Integer id) {
        Analytics analytics = findById(id);
        if (analytics != null) {
            return this.visitedMarkerHelper(analytics);
        } else {
            return false;
        }
    }

    private boolean visitedMarkerHelper(Analytics toVisit) {
        try {
            toVisit.markAsVisited();
            super.update(toVisit);
            return true;
        } catch (Exception ex) { // Whatever exception it throws doesn't matter, just return false.
            return false;
        }
    }
}
