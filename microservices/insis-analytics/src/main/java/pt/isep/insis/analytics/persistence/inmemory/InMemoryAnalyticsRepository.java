package pt.isep.insis.analytics.persistence.inmemory;

import pt.isep.insis.analytics.domain.Analytics;
import pt.isep.insis.analytics.persistence.AnalyticsRepository;

import java.util.Date;

public class InMemoryAnalyticsRepository extends InMemoryRepository<Analytics, Integer> implements AnalyticsRepository {
    protected static Integer entityCounter = 0;

    public InMemoryAnalyticsRepository() {

    }

    @Override
    public Analytics findById(Integer id) {
        Iterable<Analytics> allAnalytics = super.all();
        for (Analytics analytics : allAnalytics) {
            if (analytics.getId().equals(id)) {
                return analytics;
            }
        }
        return null;
    }

    @Override
    public Analytics findAnalytics(String campaignId, String userId) {
        Iterable<Analytics> allAnalytics = super.all();
        for (Analytics analyticsObj : allAnalytics) {
            if (analyticsObj.getCampaignId().equals(campaignId) && analyticsObj.getUserId().equals(userId)) {
                return analyticsObj;
            }
        }
        return null;
    }

    @Override
    public boolean markAsVisited(String campaignId, String userId) {
        Analytics analytics = findAnalytics(campaignId, userId);
        if (analytics == null) {
            return false;
        }
        analytics.markAsVisited();
        return true;
    }

    @Override
    public boolean markAsVisitedById(Integer id) {
        Analytics analytics = findById(id);
        return markAsVisitedHelper(analytics);
    }

    @Override
    protected Integer newPK(Analytics analytics) {
        return incrementEntityCounter();
    }

    private static synchronized Integer incrementEntityCounter() {
        return ++entityCounter;
    }

    private boolean markAsVisitedHelper(Analytics analytics) {
        if (analytics == null) {
            return false;
        }
        analytics.markAsVisited();
        return true;
    }
}
