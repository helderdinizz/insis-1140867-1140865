package pt.isep.insis.analytics.domain.DTO;

import java.util.Date;

public class AnalyticsDTO {
    public Integer id;
    public String campaignId;
    public String userId;
    public boolean visited;
    public Date visitedAt;

    public AnalyticsDTO() {
    }

    public AnalyticsDTO(String campaignId, String userId) {
        this.campaignId = campaignId;
        this.userId = userId;
    }

    public AnalyticsDTO(Integer id, String campaignId, String userId) {
        this.id = id;
        this.campaignId = campaignId;
        this.userId = userId;
    }

    public AnalyticsDTO(Integer id, String campaignId, String userId, boolean visited, Date visitedAt) {
        this.id = id;
        this.campaignId = campaignId;
        this.userId = userId;
        this.visited = visited;
        this.visitedAt = visitedAt;
    }
}
