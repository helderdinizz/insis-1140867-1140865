package pt.isep.insis.analytics.domain;

import pt.isep.insis.analytics.service.AppSettings;

public class RedirectUrlBuilder {
    public static String buildRedirectUrl(String campaignId, String userId) {
        String linkBase = AppSettings.instance().getLinkBaseUrl();
        StringBuilder sb = new StringBuilder();

        sb.append(linkBase);
        sb.append("?campaignId=");
        sb.append(campaignId);
        sb.append("&userId=");
        sb.append(userId);

        return sb.toString();
    }
}
