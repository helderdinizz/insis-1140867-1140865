package pt.isep.insis.analytics.persistence.inmemory;

import pt.isep.insis.analytics.persistence.AnalyticsRepository;
import pt.isep.insis.analytics.persistence.RepositoryFactory;

public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static AnalyticsRepository analyticsRepository = null;

    @Override
    public synchronized AnalyticsRepository analytics() {
        if (analyticsRepository == null) {
            analyticsRepository = new InMemoryAnalyticsRepository();
        }
        return analyticsRepository;
    }
}
