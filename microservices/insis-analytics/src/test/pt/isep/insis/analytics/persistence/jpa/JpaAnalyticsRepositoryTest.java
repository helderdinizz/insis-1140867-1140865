package pt.isep.insis.analytics.persistence.jpa;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pt.isep.insis.analytics.domain.Analytics;
import pt.isep.insis.analytics.persistence.PersistenceContext;

import static org.junit.Assert.*;

public class JpaAnalyticsRepositoryTest {

    private static JpaAnalyticsRepository repository;
    private static Analytics analytics1;
    private static Analytics analytics3;
    private static Analytics analytics2;
    private static Analytics analytics4;
    private static String[] mockInfo = new String[]{
            "VisitedCampaign1", "VisitedUser1",
            "VisitedCampaign2", "VisitedUser2",
            "NotVisitedCampaign3", "NotVisitedUser3",
            "NotVisitedCampaign4", "NotVisitedUser4"};

    @BeforeClass
    public static void setUp() throws Exception {
        repository = new JpaAnalyticsRepository();
        analytics1 = new Analytics(mockInfo[0], mockInfo[1]);
        analytics2 = new Analytics(mockInfo[2], mockInfo[3]);
        analytics3 = new Analytics();
        analytics3.setCampaignId(mockInfo[4]);
        analytics3.setUserId(mockInfo[5]);
        analytics4 = new Analytics();
        analytics4.setCampaignId(mockInfo[6]);
        analytics4.setUserId(mockInfo[7]);

        PersistenceContext.repositories().analytics().add(analytics1);
        PersistenceContext.repositories().analytics().add(analytics2);
        PersistenceContext.repositories().analytics().add(analytics3);
        PersistenceContext.repositories().analytics().add(analytics4);
    }

    @Test
    public void findAnalytics() {
        Analytics analytics1 = repository.findAnalytics(mockInfo[0], mockInfo[1]);
        Analytics expectedAnalytics1 = new Analytics(mockInfo[0], mockInfo[1]);

        assertEquals(analytics1, expectedAnalytics1);
    }

    @Test
    public void markAsVisited() {
        assertTrue(repository.markAsVisited(mockInfo[4], mockInfo[5]));
    }

    @Test
    public void markAsVisitedById() {
        assertTrue(repository.markAsVisitedById(analytics4.getId()));
    }
}